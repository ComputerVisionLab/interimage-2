package br.puc_rio.ele.lvc.interimage.geometry.udf;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.logicalLayer.schema.Schema;

import br.puc_rio.ele.lvc.interimage.common.GeometryParser;
import br.puc_rio.ele.lvc.interimage.common.SpatialIndex;
import br.puc_rio.ele.lvc.interimage.common.Tile;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.index.strtree.STRtree;
import com.vividsolutions.jts.io.WKTReader;

/**
 * A class that computes topological features for all the input polygons.
 * This class computes tile-based features. CombineTopologicalFeatures should be called afterwards to combine the partial values.
 * 
 * TODO: consider distance buffers
 * TODO: if distance is considered, it should be the same distance used in CalculateTiles
 *  
 * @author Rodrigo Ferreira
 */

public class TopologicalFeatures extends EvalFunc<DataBag> {

	private final GeometryParser _geometryParser = new GeometryParser();
	
	private STRtree _gridIndex = null;
	private String _gridUrl = null;
	private String _features;
	//private Double _distance = null;
	private Map<String, Map<String, Object>> _featureMap;	//attribute, operation, params
	
	/**Constructor that takes image URL, feature list and tile size.*/
	public TopologicalFeatures(String features, String gridUrl/*, String distance*/) {
		_features = features;
		_gridUrl = gridUrl;
		/*if (!distance.isEmpty())
			_distance = Double.parseDouble(distance);*/
	}
	
	private void parseFeatures() {
		
		_featureMap = new HashMap<String, Map<String, Object>>();
		
		String[] expressions = _features.split(";");
		
		for (int i=0; i<expressions.length; i++) {
			
			int idx = expressions[i].indexOf("=");
			
			String term1 = expressions[i].substring(0,idx).trim();
			
			String term2 = expressions[i].substring(idx+1).trim();
			
			int idx1 = term2.indexOf("(");
			
			String name = term2.substring(0,idx1).trim();
			
			String list = term2.substring(idx1+1,term2.length()-1).trim();
			
			String[] params = list.split(",");
						
			List<String> paramList = new ArrayList<String>();
			
			for (int j=0; j<params.length; j++) {	
				paramList.add(params[j]);				
			}
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			map.put("operation", name);
			map.put("params", paramList);
			
			_featureMap.put(term1, map);
						
		}
		
	}
	
	/**This method creates an STR-Tree index for the input bag and returns it.*/
	@SuppressWarnings("rawtypes")
	private SpatialIndex createIndex(DataBag bag) {
		
		SpatialIndex index = null;
		
		try {
		
			index = new SpatialIndex();

			Iterator it = bag.iterator();
	        while (it.hasNext()) {
	            Tuple t = (Tuple)it.next();
            	Geometry geometry = _geometryParser.parseGeometry(t.get(0));            	            	
				index.insert(geometry.getEnvelopeInternal(),t);					
	        }
		} catch (Exception e) {
			System.err.println("Failed to index bag; error - " + e.getMessage());
			return null;
		}
		
		return index;
	}
	
	/**
     * Method invoked on every bag during foreach evaluation.
     * @param input tuple<br>
     * first column is assumed to have a bag
     * @exception java.io.IOException
     * @return a bag with the computed features in the properties
     */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public DataBag exec(Tuple input) throws IOException {
				
		if (input == null || input.size() == 0)
            return null;
			
		//executes initialization
		if (_gridIndex == null) {
			_gridIndex = new STRtree();
			
			//Creates an index for the grid
	        try {
	        	
	        	if (!_gridUrl.isEmpty()) {
	        		
	        		URL url  = new URL(_gridUrl);	        		
	                URLConnection urlConn = url.openConnection();
	                urlConn.connect();
			        InputStream buff = new BufferedInputStream(urlConn.getInputStream());				    	    	        
			        ObjectInputStream in = new ObjectInputStream(buff);
	    			
	    		    List<Tile> tiles = (List<Tile>)in.readObject();
	    		    
	    		    in.close();
				    
				    for (Tile t : tiles) {
				    	Geometry geometry = new WKTReader().read(t.getGeometry());
			    		_gridIndex.insert(geometry.getEnvelopeInternal(),t);				    	
				    }
			        			        
	        	}
	        } catch (Exception e) {
				throw new IOException("Caught exception reading grid file ", e);
			}
	       	        
		}
		
		try {
		
			DataBag bag = DataType.toBag(input.get(0));
						
			DataBag output = BagFactory.getInstance().newDefaultBag();
				
			if (_featureMap == null) {
				parseFeatures();
			}
			
			SpatialIndex index = createIndex(bag);
			
			Iterator it = bag.iterator();
	        while (it.hasNext()) {
	            Tuple t = (Tuple)it.next();
            	Geometry geometry = _geometryParser.parseGeometry(t.get(0));            	            	
            	Map<String,Object> props = DataType.toMap(t.get(2));
            	
            	/*if (_distance != null)
            		geometry = geometry.buffer(_distance);*/
            	
            	List<Tile> tiles = _gridIndex.query(geometry.getEnvelopeInternal());
            	
            	List<Tuple> neighbors = index.query(geometry.getEnvelopeInternal());
            	
            	Map<String, Map<String, Object>> features = new HashMap<String, Map<String, Object>>();
            	
            	for (Tuple n : neighbors) {
            		            		
    				Geometry ngeometry = _geometryParser.parseGeometry(n.get(0));
            		
    				if (ngeometry.intersects(geometry)) {
    				
	    				Map<String,Object> nprops = DataType.toMap(n.get(2));
	    				
	    				String tile = DataType.toString(nprops.get("tile"));
	            		
	    				String className = DataType.toString(nprops.get("class"));
	    				
	            		List<Tile> ntiles = _gridIndex.query(ngeometry.getEnvelopeInternal());
	            		
	            		//compute similar tiles list
	            		tiles.retainAll(ntiles);
	            		
	            		//compute minimum similar tile
	            		
	            		String min = null;
	    				
	    				for (Tile k : tiles) {
	    					
	    					String ts = k.getCode();
	    					
	    					if (min.isEmpty()) {
	    						min = ts;
	    					} else {
	    						if (ts.compareTo(min) < 0) {
	    							min = ts;
	    						}
	    					}
	    				}
	            		
	            		if (tile.equals(min)) {
	            			
	            			//compute features
	            			
	            			for (Map.Entry<String, Map<String, Object>> entry : _featureMap.entrySet()) {
	            				
	        					String feature = entry.getKey();
	        					String operation = (String)entry.getValue().get("operation");
	        					List<String> paramList = (List<String>)entry.getValue().get("params");
	        					
	        					if (operation.equals("numberOf")) {
	        						
	        						if (paramList.get(0).equals(className)) {
	        							
	        							if (features.containsKey(feature)) {
	        								Map<String,Object> m = features.get(feature);
	        								Double aux = (Double)m.get("sum");
	        								m.put("sum", aux + 1.0);
	        							} else {
	        								Map<String,Object> m = new HashMap<String,Object>();
	        								m.put("sum", 1.0);
	        								features.put(feature, m);
	        							}
	        							
	        						}
	        						
	        					} else if (operation.equals("borderTo")) {
	        						
	        						if (paramList.get(0).equals(className)) {
	        							
	        							Geometry geom = geometry.intersection(ngeometry);
	        							Double borderTo = geom.getLength();
	        							
	        							if (features.containsKey(feature)) {
	        								Map<String,Object> m = features.get(feature);
	        								Double aux = (Double)m.get("sum");
	        								m.put("sum", aux + borderTo);
	        							} else {
	        								Map<String,Object> m = new HashMap<String,Object>();
	        								m.put("sum", borderTo);
	        								features.put(feature, m);
	        							}
	        							
	        						}
	        						
	        					} else if (operation.equals("relativeBorderTo")) {
	        						
	        						if (paramList.get(0).equals(className)) {
	        							
	        							Geometry geom = geometry.intersection(ngeometry);
	        							Double relBorderTo = geom.getLength() / geometry.getLength();
	        							
	        							if (features.containsKey(feature)) {
	        								Map<String,Object> m = features.get(feature);
	        								Double aux = (Double)m.get("sum");
	        								m.put("sum", aux + relBorderTo);
	        							} else {
	        								Map<String,Object> m = new HashMap<String,Object>();
	        								m.put("sum", relBorderTo);
	        								features.put(feature, m);
	        							}
	        							
	        						}
	        						
	        					} else if (operation.equals("areaOf")) {
	        						
	        						if (paramList.get(0).equals(className)) {
	        							
	        							Double areaOf = ngeometry.getArea();
	        							
	        							if (features.containsKey(feature)) {
	        								Map<String,Object> m = features.get(feature);
	        								Double aux = (Double)m.get("sum");
	        								m.put("sum", aux + areaOf);
	        							} else {
	        								Map<String,Object> m = new HashMap<String,Object>();
	        								m.put("sum", areaOf);
	        								features.put(feature, m);
	        							}
	        							
	        						}
	        						
	        					} else if (operation.equals("relativeAreaOf")) {
	        						
	        						if (paramList.get(0).equals(className)) {
	        							
	        							Double realAreaOf = ngeometry.getArea() / geometry.getArea();
	        							
	        							if (features.containsKey(feature)) {
	        								Map<String,Object> m = features.get(feature);
	        								Double aux = (Double)m.get("sum");
	        								m.put("sum", aux + realAreaOf);
	        							} else {
	        								Map<String,Object> m = new HashMap<String,Object>();
	        								m.put("sum", realAreaOf);
	        								features.put(feature, m);
	        							}
	        							
	        						}
	        						
	        					}
	        					
	            			}
	            			
	            		}
	            		
    				}
            		
            	}
            
            	props.put("topological_features", features);
				
				String orig_tile = DataType.toString(props.get("orig_tile"));
				
				props.put("tile", orig_tile);
				
				props.remove("orig_tile");
				            	
            	output.add(t);
            	
	        }
			
	        return output;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new IOException("Caught exception processing input row ", e);
		}
		
	}
	
	@Override
    public Schema outputSchema(Schema input) {
		
		try {

			List<Schema.FieldSchema> list = new ArrayList<Schema.FieldSchema>();
			list.add(new Schema.FieldSchema(null, DataType.CHARARRAY));
			list.add(new Schema.FieldSchema(null, DataType.MAP));
			list.add(new Schema.FieldSchema(null, DataType.MAP));
					
			Schema tupleSchema = new Schema(list);
			
			Schema.FieldSchema ts = new Schema.FieldSchema(null, tupleSchema, DataType.TUPLE);
			
			Schema bagSchema = new Schema(ts);
			
			Schema.FieldSchema bs = new Schema.FieldSchema(null, bagSchema, DataType.BAG);
			
			return new Schema(bs);

		} catch (Exception e) {
			return null;
		}
		
    }
	
}
