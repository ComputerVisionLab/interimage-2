/*Copyright 2014 Computer Vision Lab

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package br.puc_rio.ele.lvc.interimage.geometry.udf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.logicalLayer.schema.Schema;

/**
 * A class that combines partial values of topological features.<br>
 * This class is not meant to be used alone. It should be used after TopologicalFeatures.<br><br>
 * 
 * Constraints:<br>
 * If numberOf is needed, this UDF should be called after a MergeNeighbors operation.
 * 
 * 
 * @author Rodrigo Ferreira
 */

public class CombineTopologicalFeatures extends EvalFunc<DataBag> {

	//private final GeometryParser _geometryParser = new GeometryParser();
	
	@SuppressWarnings({ "rawtypes" })
	private void combineFeatures(DataBag bag, DataBag output) {
				
		try {
			
			Map<String, List<Tuple>> tupleMap = new HashMap<String, List<Tuple>>();
			Map<String, Tuple> finalMap = new HashMap<String, Tuple>();
												
			Iterator it = bag.iterator();
		    while (it.hasNext()) {
		        Tuple t = (Tuple)it.next();
		        
		        //Geometry geometry = _geometryParser.parseGeometry(t.get(0));		        
		        Map<String,Object> props = DataType.toMap(t.get(2));				
				
		        String iiuuid = DataType.toString(props.get("iiuuid"));
		        		        
		        if (tupleMap.containsKey(iiuuid)) {
		        	List<Tuple> l = tupleMap.get(iiuuid);
		        	l.add(t);
		        } else {
		        	List<Tuple> l = new ArrayList<Tuple>();
		        	l.add(t);
		        	tupleMap.put(iiuuid, l);
		        }
		       		        		        
		        if (!finalMap.containsKey(iiuuid)) {
		        	if (!props.containsKey("iirep"))
		        		finalMap.put(iiuuid, t);
		        }
		      		        		        
		    }
		    		    
		    //compute features
		    
		    for (Map.Entry<String, List<Tuple>> entry1 : tupleMap.entrySet()) {
		    	
		    	String iiuuid = entry1.getKey();
		    	
		    	Map<String, Map<String, Object>> featureMap = new HashMap<String, Map<String, Object>>();
		    	
		    	List<Tuple> list = entry1.getValue();
		    	
		    	for (Tuple t : list) {
		    	
		    		Map<String,Object> props = DataType.toMap(t.get(2));
		    		
			    	Map<String, Object> topological_features = DataType.toMap(props.get("topological_features"));
			        
			        for (Map.Entry<String, Object> entry2 : topological_features.entrySet()) {
			        	
			        	String name = entry2.getKey();
			        	
			        	Map<String, Object> params = DataType.toMap(entry2.getValue());
			        	
			        	String operation = DataType.toString(params.get("name"));
			        	
			        	//TODO: tile and image info could be used to combine GLCM texture features
			        	
			        	//String tile = (String)params.get("tile");
			        	//String image = (String)params.get("image");
			        	
			        	if (operation.equals("numberOf")) {
			        		
			        		Double sum = DataType.toDouble(params.get("sum"));
			        				        						        		
			        		if (featureMap.containsKey(name)) {
			        			Map<String, Object> m = featureMap.get(name);
			        			
			        			Double s = (Double)m.get("sum");			        			
			        			
			        			m.put("sum", s + sum);
			        			
			        		} else {
			        			
			        			HashMap<String, Object> m = new HashMap<String, Object>();
			        						        			
			        			m.put("sum", sum);
			        			
			        			m.put("name", operation);
			        			
			        			featureMap.put(name, m);
			        			
			        		}
			        		
			        	} else if (operation.equals("borderTo")) {
			        		
			        		Double sum = DataType.toDouble(params.get("sum"));
			        		
			        		if (featureMap.containsKey(name)) {
			        			Map<String, Object> m = featureMap.get(name);
			        			
			        			Double s = (Double)m.get("sum");
			        						        			
			        			m.put("sum", s + sum);
			        						        			
			        		} else {
			        			
			        			HashMap<String, Object> m = new HashMap<String, Object>();
			        			
			        			m.put("sum", sum);
			        			
			        			m.put("name", operation);
			        						        			
			        			featureMap.put(name, m);
			        			
			        		}
			        		
			        	} else if (operation.equals("relativeBorderTo")) {
			        		
			        		Double sum = DataType.toDouble(params.get("sum"));
			        		
			        		if (featureMap.containsKey(name)) {
			        			Map<String, Object> m = featureMap.get(name);
			        			
			        			Double s = (Double)m.get("sum");
			        						        			
			        			m.put("sum", s + sum);
			        						        			
			        		} else {
			        			
			        			HashMap<String, Object> m = new HashMap<String, Object>();
			        			
			        			m.put("sum", sum);
			        			
			        			m.put("name", operation);
			        						        			
			        			featureMap.put(name, m);
			        			
			        		}
			        		
			        	} else if (operation.equals("areaOf")) {
			        		
			        		Double sum = DataType.toDouble(params.get("sum"));
		        			
			        		if (featureMap.containsKey(name)) {
			        			Map<String, Object> m = featureMap.get(name);
			        			
			        			Double s = (Double)m.get("sum");
			        						        			
			        			m.put("sum", s + sum);
			        			
			        		} else {
			        			
			        			HashMap<String, Object> m = new HashMap<String, Object>();
			        			
			        			m.put("sum", sum);
			        			
			        			m.put("name", operation);
			        						        			
			        			featureMap.put(name, m);
			        			
			        		}
			        		
			        	} else if (operation.equals("relativeAreaOf")) {
			        					        		
			        		Double sum = DataType.toDouble(params.get("sum"));
		        			
			        		if (featureMap.containsKey(name)) {
			        			Map<String, Object> m = featureMap.get(name);
			        			
			        			Double s = (Double)m.get("sum");
			        						        			
			        			m.put("sum", s + sum);
			        			
			        		} else {
			        			
			        			HashMap<String, Object> m = new HashMap<String, Object>();
			        			
			        			m.put("sum", sum);
			        			
			        			m.put("name", operation);
			        						        			
			        			featureMap.put(name, m);
			        			
			        		}
			        		
			        	} 
			        	
			        }
			        
		    	}
			    
		    	//combine features
		    	
		    	Tuple t = finalMap.get(iiuuid);
		    	
		    	Map<String,Object> props = DataType.toMap(t.get(2));
		    	
		    	for (Map.Entry<String, Map<String, Object>> entry3 : featureMap.entrySet()) {
		    	
		    		String feature = entry3.getKey();
		    		Map<String, Object> m = entry3.getValue();
		    		
		    		String operation = (String)m.get("name");
		    		
		    		if (operation.equals("numberOf")) {
		    			
		    			Double sum = (Double)m.get("sum");
		    			
	    				props.put(feature, sum);
		    			
		    		} else if (operation.equals("borderTo")) {
		    			
		    			Double sum = (Double)m.get("sum");
		    			
	    				props.put(feature, sum);
		    			
		    		} else if (operation.equals("relativeBorderTo")) {
		    			
		    			Double sum = (Double)m.get("sum");
		    			
	    				props.put(feature, sum);
		    					    			
		    		} else if (operation.equals("areaOf")) {
		    			
		    			Double sum = (Double)m.get("sum");
		    			
	    				props.put(feature, sum);
		    			
		    		} else if (operation.equals("relativeAreaOf")) {
		    			
		    			Double sum = (Double)m.get("sum");
		    			
	    				props.put(feature, sum);		    			
		    			
		    		}
		    				    	
		    	}
		    	
		    	props.remove("topological_features");
		    	
		    	t.set(2, props);
		    	output.add(t);		    	
		    			    	
			}
		    		    
		    
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("It was not possible to combine the topological features.");	
		}
				
	}
			
	/**
     * Method invoked on every bag during foreach evaluation.
     * @param input tuple<br>
     * first column is assumed to have a bag with the single parent object
     * second column is assumed to have a bag with the corresponding child objects
     * @exception java.io.IOException
     * @return a bag with the computed features in the properties
     */
	@Override
	public DataBag exec(Tuple input) throws IOException {
				
		if (input == null || input.size() == 0)
            return null;
			
		try {
			
			DataBag bag = DataType.toBag(input.get(0));
			
			DataBag output = BagFactory.getInstance().newDefaultBag();
	        
			combineFeatures(bag, output);
			
	        return output;
	        
		} catch (Exception e) {
			e.printStackTrace();
			throw new IOException("Caught exception processing input row ", e);
		}
	}
		
	@Override
    public Schema outputSchema(Schema input) {
		
		try {

			List<Schema.FieldSchema> list = new ArrayList<Schema.FieldSchema>();
			list.add(new Schema.FieldSchema(null, DataType.CHARARRAY));
			list.add(new Schema.FieldSchema(null, DataType.MAP));
			list.add(new Schema.FieldSchema(null, DataType.MAP));
					
			Schema tupleSchema = new Schema(list);
			
			Schema.FieldSchema ts = new Schema.FieldSchema(null, tupleSchema, DataType.TUPLE);
			
			Schema bagSchema = new Schema(ts);
			
			Schema.FieldSchema bs = new Schema.FieldSchema(null, bagSchema, DataType.BAG);
			
			return new Schema(bs);

		} catch (Exception e) {
			return null;
		}
		
    }
	
}
