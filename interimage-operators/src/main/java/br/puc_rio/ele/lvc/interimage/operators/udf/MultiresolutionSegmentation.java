/*Copyright 2014 Computer Vision Lab

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package br.puc_rio.ele.lvc.interimage.operators.udf;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.stream.ImageInputStream;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.schema.Schema;

import br.puc_rio.ele.lvc.interimage.common.Tile;
import br.puc_rio.ele.lvc.interimage.common.TileManager;
import br.puc_rio.ele.lvc.interimage.common.UUID;
import br.puc_rio.ele.lvc.interimage.operators.MultiSegment;
import br.puc_rio.ele.lvc.interimage.data.Image;
import br.puc_rio.ele.lvc.interimage.data.imageioimpl.plugins.tiff.TIFFImageReader;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.index.strtree.STRtree;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.io.WKTWriter;

//TODO: This could be a generic UDF that receives the parameters and compute a particular segmentation process.
//TODO: Create an interface for segmentation and then each implementation

//NOTES:
//Merging Heuristic: Best Fitting/ Local Mutual Best Fitting (parameter)
//Merging Cost Computation: spectral + smoothness + compactness 

//Removing from List is more slow!
//So, begins with almost the total of memory required and keeps using it - but adds bestNeighbors!
//A segment that is not valid, i.e. has been incorporated by another, will not have pixels and will not be processed anymore.
//Can delete current PixelList and perimeter computation if do not use perimeter in morphological features

//NON MUTUAL
//Uses an array (_visitingOrder) in order to set the visiting order
//A segment is visited only once at each step. But a visited segment can merge with one that has already merged!

//MUTUAL
//Compute merging cost of lower neighbors and adds the value to them avoiding duplicate processing
//Uses a list of best neighbors (more than 1 is possible when the merge costs are the same)

/**
 * UDF for MultiResolution Segmentation.
 * @author Patrick Happ, Rodrigo Ferreira
 */

public class MultiresolutionSegmentation extends EvalFunc<DataBag> {
	//Initializing threshold with default value
	//private static double THRESHOLD=0.02;
	
	private static int ConnAttempts = 10; //number of connection attempts before fail
	
	//private final GeometryParser _geometryParser = new GeometryParser();
	//private Double _segmentSize;
	private String _imageUrl;
	private String _image;
	//private STRtree _roiIndex = null;
	//private String _roiUrl = null;
	
	static ArrayList <MultiSegment> _segmentsPtr;
	static int [] _visitingOrder; // only used by non mutual
	
	private static int _nbands;
	private static int _imageH;
	private static int _imageW;
	
	private static double _scale; //this is scale^2!!
	private static double _wColor;
	private static double _wCmpt;
	private static double [] _wBand;
	private static boolean _mutual; //Mutual: 0 - Best fitting; 1-Local Mutual Best Fitting
	private static int _type; //Type:0 - normal, 1 - post-processing, 2 - stable margin
	private static int _maxSteps;
	private static double _tileSize = 512; //tile size 
	private static double _resolution =1; //resolution
	
	private static String _gridUrl = null;
	private static STRtree _gridIndex = null;
	
	public MultiresolutionSegmentation (String imageUrl, String image, String scale, String wColor, String wCmpt, String wBands, String mutual, String type, String maxSteps, String gridUrl, String tileSize, String resolution) {
		//_segmentSize = Double.parseDouble(segmentSize);
		_imageUrl = imageUrl;
		_image = image;
		//_roiUrl = roiUrl;
		
		_scale = Math.pow(Double.parseDouble(scale),2);
		_wColor = Double.parseDouble(wColor);
		_wCmpt = Double.parseDouble(wCmpt);
		
		_segmentsPtr = null;
		
		_nbands=0;
		_imageH=0;
		_imageW=0;
		
		/*Reading and normalizing band weights*/
		String[] bands = wBands.split(",");

		double sum = 0.0;
		
		_wBand = new double[bands.length];
		for (int i=0; i<bands.length; i++) {
			sum = sum + Double.parseDouble(bands[i]);
		}
		
		for (int i=0; i<bands.length; i++) {
			_wBand[i] = Double.parseDouble(bands[i]) / sum;
		}
		
		if (Integer.parseInt(mutual)==0){
			_mutual = false;
		} else {
			_mutual = true;
		}
		
		if (!type.isEmpty())
			_type = Integer.parseInt(type);
		else
			_type = 0; //normal execution

		
		if (!maxSteps.isEmpty())
			_maxSteps = Integer.parseInt(maxSteps);
		else
			_maxSteps = 10000; //would not execute more iterations than that
		
		//Check the gridUrl
		if (!gridUrl.isEmpty())
			_gridUrl = gridUrl;
		
		//Check the _tileSize
		if (!tileSize.isEmpty())
			_tileSize = Double.parseDouble(tileSize);
		
		if (!resolution.isEmpty()){
			_resolution = Double.parseDouble(resolution);
		}
	}
	
	/**
     * Method invoked on every tuple during foreach evaluation.
     * @param input tuple<br>
     * first column is assumed to have the geometry<br>
     * second column is assumed to have the data<br>
     * third column is assumed to have the properties
     * @exception java.io.IOException
     * @return a bag with the polygons created by the segmentation
     */
	@SuppressWarnings("unchecked")
	@Override
	public DataBag exec(Tuple input) throws IOException {
		if (input == null || input.size() < 3)
            return null;
		
		//Get Index of tiles
			if (_gridIndex == null) {
				_gridIndex = new STRtree();
				
				//Creates an index for the grid
		        try {
		        	
		        	if (!_gridUrl.isEmpty()) {
		        		
		        		URL url  = new URL(_gridUrl);	
		        		
		        		URLConnection urlConn  = null;
			        	InputStream buff = null;
			        	ObjectInputStream in = null;
			        	
			        	List<Tile> tiles = null;
			        	
			        	for (int i=0; i < ConnAttempts; i++) {
			        		try{
			        			
			        			urlConn = url.openConnection();
			        			
			        			urlConn.setReadTimeout(50000);
			        			urlConn.setConnectTimeout(5000);
			        			urlConn.connect();
			                    buff = new BufferedInputStream(urlConn.getInputStream());
			                    in =  new ObjectInputStream(buff);
			                    
			                    tiles = (List<Tile>)in.readObject();
			        		}
			        		catch (IOException e){
			        			continue;
			        		}
			        		
			        		break;
			        	}
		        		
		    		
		    		    					    
					    for (Tile t : tiles) {
					    	Geometry geometry = new WKTReader().read(t.getGeometry());
	    					//_gridIndex.insert(geometry.getEnvelopeInternal(),t);
	    					_gridIndex.insert(geometry.buffer(_resolution*0.5).getEnvelopeInternal(),t);
					    }
					    
					    in.close();
					    buff.close();				        			        
		        	}
		        } catch (Exception e) {
					throw new IOException("Caught exception reading grid file " + e.getMessage());
				}
		       	        
			}
		
		try {		
			//Object objGeometry = input.get(0);
			Map<String,String> data = (Map<String,String>)input.get(1);
			Map<String,Object> properties = DataType.toMap(input.get(2));
			
			DataBag bag = BagFactory.getInstance().newDefaultBag();
			//Geometry inputGeometry = _geometryParser.parseGeometry(objGeometry);
			
			String tileStr = DataType.toString(properties.get("tile"));
			String crs = DataType.toString(properties.get("crs"));
				
				_segmentsPtr = new ArrayList <MultiSegment>();
	        	
				//read image and create seeds
				//System.out.println("LOG: Initialize begin");
	        	initializeSegments(_imageUrl, _image, tileStr, crs);
	        	//System.out.println("LOG: Initialize end");
				
	        	//System.out.println("LOG: Segmentation begin");
	        	//iterates over the segments and do the region growing
	        	if (_mutual)
	    			computeSegmentation();
	    		else{
	    			randomOrder();
	    			computeSegmentationNotMutual();
	    		}   	
	        	//System.out.println("LOG: Segmentation end");	    
	        	
	        	//Write Results
	        	
	        	//Get Geocoordinates
	        	double[] imageTileGeoBox = new double[4];
	        	URL worldFile1 = new URL(_imageUrl + _image + "/" + tileStr + ".meta");
	        	
	        	URLConnection urlConn1  = null;
	        	InputStreamReader inStream1 = null;
	        	
	        	for (int i=0; i < ConnAttempts; i++) {
	        		
	        		try{
	        			
	        			urlConn1 = worldFile1.openConnection();
	        			
	        			urlConn1.setReadTimeout(50000);
	        			urlConn1.setConnectTimeout(5000);
	                    urlConn1.connect();
	    				inStream1 = new InputStreamReader(urlConn1.getInputStream());
	        		}
	        		catch (IOException e){
	        			continue;
	        		}
	        		
	        		break;
	        	}
	        	
		        BufferedReader reader1 = new BufferedReader(inStream1);
		        		        
		        String line1;
		        int index1 = 0;
		        while ((line1 = reader1.readLine()) != null) {
		        	if (!line1.trim().isEmpty()) {
		        		if (index1==3)
		        			imageTileGeoBox[0] = Double.parseDouble(line1);
		        		else if (index1==4)
		        			imageTileGeoBox[1] = Double.parseDouble(line1);
		        		else if (index1==5)
		        			imageTileGeoBox[2] = Double.parseDouble(line1);
		        		else if (index1==6)
		        			imageTileGeoBox[3] = Double.parseDouble(line1);
			        	index1++;
		        	}
		        }
		        reader1.close();
		        inStream1.close();
		        
		        //double resX = (imageTileGeoBox[2]-imageTileGeoBox[0])/_imageW;
		        /*Using a threshold that represents one tenth of the resolution*/
		        //THRESHOLD = resX / 10;
		        
			    //ArrayList< ArrayList<double[]> > rings = new ArrayList< ArrayList<double[]> >();
			    //double CoordX, CoordY, CoordX2, CoordY2;
		        
		        GeometryFactory fact = new GeometryFactory();
		        WKTWriter writer = new WKTWriter();
		        UUID uuid = new UUID(null);
		        		
		        List<Geometry> list = new ArrayList<Geometry>();
		        
		        int idSeg=0;
		        int auxPixel;
		        //System.out.println("LOG: Write Results begin");
			    for (MultiSegment auxSegment : _segmentsPtr) {
			    	 //int [] outterRing = {0};
			    	 //rings.clear();
			    	
			    	if (idSeg == auxSegment.getId()){
			    		auxPixel = idSeg;
				    	if (auxPixel != -1){
				    					    	
					    	while (auxPixel != -1) {

					    		double CoordX, CoordY;
								int x = auxPixel % _imageW;
								int y = auxPixel / _imageW;
								 
			      				Coordinate [] linePoints = new Coordinate[5];
								//left top corner
			      				CoordX = Image.imgToGeoX(x - 0.5, _imageW, imageTileGeoBox);
			      				CoordY = Image.imgToGeoY(y - 0.5 ,_imageH, imageTileGeoBox);
			      				linePoints[0]= new Coordinate(CoordX,CoordY);
			      				linePoints[4]= new Coordinate(CoordX,CoordY); //close the ring
				                //right top corner
			      				CoordX = Image.imgToGeoX(x + 0.5, _imageW,imageTileGeoBox);
			      				CoordY = Image.imgToGeoY(y - 0.5,_imageH,imageTileGeoBox);
			      				linePoints[1] = new Coordinate(CoordX,CoordY);
								//right bottom corner
								CoordX = Image.imgToGeoX(x + 0.5,_imageW,imageTileGeoBox);
								CoordY = Image.imgToGeoY(y + 0.5,_imageH,imageTileGeoBox);
								linePoints[2] = new Coordinate(CoordX,CoordY);
								//right bottom corner
								CoordX = Image.imgToGeoX(x - 0.5,_imageW,imageTileGeoBox);
								CoordY = Image.imgToGeoY(y + 0.5,_imageH,imageTileGeoBox);
								linePoints[3] = new Coordinate(CoordX,CoordY);
								 
								LinearRing shell = fact.createLinearRing(linePoints);		          			
			          			//Geometry poly = new Polygon(shell, null, fact);
								Geometry poly = fact.createPolygon(shell, null);
								 
			          			list.add(poly);
			          			
			          			auxPixel = _segmentsPtr.get(auxPixel).getNextPixelId();
			          			
					        }
			        		
				        	Geometry[] geoms = new Geometry[list.size()];
				        	
				        	int index = 0;
				        	for (Geometry geom : list) {
				        		geoms[index] = geom;
				        		index++;
				        	}
				        	
				        	//TODO: union seems slightly faster than buffer(0)
				        	//TODO: Passing the factory as a parameter here created a huge performance issue
				        	//For now, calling the method from the factory, much faster
				        	//Geometry union = new GeometryCollection(geoms, fact).buffer(0);
				        	Geometry union = fact.createGeometryCollection(geoms).buffer(0);
				        	
							Tuple t = TupleFactory.getInstance().newTuple(3);

							Map<String,Object> props = new HashMap<String,Object>(properties);
			        		String id = uuid.random();	
			        		props.put("iiuuid", id);
							
			        		boolean active=true; //only used for stable margin validation
			        		
			        		//One-Step Post-Processing
							String groupId= tileStr;
							List<Tile> tiles = _gridIndex.query(union.getEnvelopeInternal());
							String tileBorder="false"; //keep track of boundary segments
							//System.out.println("GROUPID: " + groupId);
							for (Tile itTile : tiles) {
								String tileId = itTile.getCode();
								//System.out.println("TILEID: " + tileId);
								if (!tileId.equals(groupId)){
									tileBorder = "true";
									//System.out.println("DELAYED");
									if  (tileId.compareTo(groupId)>0){//TODO: May verify the largest intersection in order to choose the GroupId
										groupId=tileId;
									}
								}
							}
							props.put("tileBorder", tileBorder);
							props.put("GroupID", groupId);
							
							//TODO: Only border objects must have properties!! -> put inside IF
							props.put("Area",auxSegment.getArea());
							String[] name={"Mean_", "Square_", "Sum_", "Std_"};
							for (int i=0; i<_wBand.length;i++){
								props.put(name[0].concat(String.valueOf(i)),auxSegment.getAvg_colorByIndex(i));
								props.put(name[1].concat(String.valueOf(i)),auxSegment.getAvg_color_squareByIndex(i));
								props.put(name[2].concat(String.valueOf(i)),auxSegment.getColor_sumByIndex(i));
								props.put(name[3].concat(String.valueOf(i)),auxSegment.getStd_colorByIndex(i));
							}
							props.put("Area",auxSegment.getArea());
							    		
			        		if (active){ //all objects are valid except when stable margin is used (tests are applied)
				        		t.set(0,writer.write(union));
				        		t.set(1,new HashMap<String,String>(data));
				        		t.set(2,props);
				        		bag.add(t);
			        		}
			        		geoms = null;
			        		list.clear();
			        		
				    	}
			    	}
			    	idSeg++; 
			   }
			    //System.out.println("LOG: Write Results end");
			    _segmentsPtr.clear();
		        
			return bag;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new IOException("Caught exception processing input row " + e.getMessage(), e);
		}
	}
	
	@Override
    public Schema outputSchema(Schema input) {
	        
			try {

			List<Schema.FieldSchema> list = new ArrayList<Schema.FieldSchema>();
			list.add(new Schema.FieldSchema(null, DataType.CHARARRAY));
			list.add(new Schema.FieldSchema(null, DataType.MAP));
			list.add(new Schema.FieldSchema(null, DataType.MAP));
			
			Schema tupleSchema = new Schema(list);
			
			Schema.FieldSchema ts = new Schema.FieldSchema(null, tupleSchema, DataType.TUPLE);
			
			Schema bagSchema = new Schema(ts);
			
			Schema.FieldSchema bs = new Schema.FieldSchema(null, bagSchema, DataType.BAG);
			
			return new Schema(bs);

		} catch (Exception e) {
			return null;
		}
		
    }
	
	private static void initializeSegments(String imageUrl, String image, String tileStr, String crs) throws Exception{	
		BufferedImage buff = null;
		
		
		//NORMAL EXECUTION
		//Read Image
		String imageFile = imageUrl + image + "/" + tileStr + ".tif";
		URL worldFile1 = new URL(imageFile);

		URLConnection urlConn = null;
    	InputStream stream = null;
    	ImageInputStream in = null;
    	
    	TIFFImageReader reader = null;
    	ImageReadParam param = null;
    	
    	for (int i=0; i < ConnAttempts; i++) {
    		
    		try{
    			
    			urlConn = worldFile1.openConnection();
    			
    			urlConn.setReadTimeout(50000);
    			urlConn.setConnectTimeout(5000);
    			urlConn.connect();
    			
    			stream = new BufferedInputStream(urlConn.getInputStream());
    			in = ImageIO.createImageInputStream(stream);
    			
			   if (in == null)
		        	throw new Exception("Could not create input stream: " + imageFile.toString());
		        
		        reader = new TIFFImageReader(null);
		        reader.setInput(in);
				
		        param = reader.getDefaultReadParam();       
		        buff = reader.read(0,param);
    		}
    		catch (IOException e){
    			continue;
    		}
    		
    		break;
    	}
    	

        if (buff == null)
			throw new Exception("Could not instantiate tile image: " + imageFile.toString());

        _imageH=reader.getHeight(0);
        _imageW=reader.getWidth(0);

		WritableRaster raster = buff.getRaster();
        _nbands=raster.getNumBands();
        
        if (_nbands != _wBand.length){
        	throw new Exception("Image bands are incompatible with band weights parameter");
        }
        
        MultiSegment auxSegment;
        int id=0;
        //for each line
        for (int row = 0; row < _imageH; row++) {          	          	
          	//for each pixel
          	for (int col=0; col<_imageW; col++){
          		
	          	//Create a segment for the pixel
          		auxSegment = MultiSegment.create(id);
          		_segmentsPtr.add(auxSegment);
          		
          		
          		//auxSegment.id = idPixel;
				auxSegment.setB_boxByIndex((double)row, 0);
				auxSegment.setB_boxByIndex((double)col, 1);

				auxSegment.createSpectral(_nbands);
				double val;
				for(int b = 0; b < _nbands; b++)
				{
					val = raster.getSampleDouble(col, row, b);
					auxSegment.setAvg_colorByIndex(val, b);
					auxSegment.setStd_colorByIndex(0, b);
					auxSegment.setAvg_color_squareByIndex(val*val, b);
					auxSegment.setColor_sumByIndex(val, b);
				}
								
				//Set Pixel
				auxSegment.setBorderline(true);
				auxSegment.setLastPixelId(id);
				auxSegment.setNextPixelId(-1);

				id++;
          	}
      }
        buff.flush();
	}

	private static void computeSegmentation(){
		MultiSegment currNeighbor = null;
		int auxPixel = -1;
		
		ArrayList <Integer> nbSeg= new ArrayList <Integer>();
		
		boolean hasMerged=true;
		int step=0;
		while (hasMerged && step < _maxSteps){
			hasMerged=false;
			
			int idSeg=0;
			for (MultiSegment currSegment : _segmentsPtr){			
				//If it is a valid segment
				
				if (idSeg == currSegment.getId()){
					auxPixel = idSeg;

					/* for each outline pixel */
					while (auxPixel!=-1)
					{
						if (_segmentsPtr.get(auxPixel).isBorderline()){
							int [] nb = getPixelIdFromNeighbors(auxPixel);
							  
							for (int n=0;n<4;n++)
							{
								if (nb[n] != -1)
								{
									nb[n] = _segmentsPtr.get(nb[n]).getId();
									//if neighbor has lower id (compute just lower IDs)
									if (nb[n]<idSeg){
										//if it has not been selected yet
										if (!nbSeg.contains(nb[n])){
											nbSeg.add(nb[n]);
										}
									}
								}
							}
						 }
						//next pixel
						 auxPixel = _segmentsPtr.get(auxPixel).getNextPixelId();
					}
					
					//get merging costs
					for (int nbId: nbSeg){
						currNeighbor = _segmentsPtr.get(nbId);
						double cost = GetCost(currSegment, currNeighbor);
						//test for the current segment
						if (cost < currSegment.getMergingCost()){
							currSegment.setMergingCost(cost);
							currSegment.getBestNb().clear();
							currSegment.getBestNb().add(nbId);
						}
						else if (cost == currSegment.getMergingCost()){
							//LIST
							currSegment.getBestNb().add(nbId);
							//or UNIQUE
//							if (nbId < currSegment.getBestNb().get(0)){
//								currSegment.getBestNb().clear();
//								currSegment.getBestNb().add(nbId);
//							}
						}
						//test for the neighbor segment
						if (cost < currNeighbor.getMergingCost()){
							currNeighbor.setMergingCost(cost);
							currNeighbor.getBestNb().clear();
							currNeighbor.getBestNb().add(idSeg);
						} 
						else if (cost == currSegment.getMergingCost()){
							//LIST
							currNeighbor.getBestNb().add(idSeg);
							//or UNIQUE
//							if (idSeg < currNeighbor.getBestNb().get(0)){
//								currNeighbor.getBestNb().clear();
//								currNeighbor.getBestNb().add(nbId);
//							}
						}
					}
					nbSeg.clear();
				}
				idSeg++;
			}
			idSeg=0;	
			//***Merge mutual segments
			for (MultiSegment currSegment : _segmentsPtr){
				if (idSeg == currSegment.getId()){
					
					//int bestNb = currSegment.getBestNb();
					//if (bestNb != -1){
					
					if (currSegment.getMergingCost() <= _scale){
						//test for mutual
						for(int bestNb : currSegment.getBestNb() ){
							currNeighbor = _segmentsPtr.get(bestNb);
							if (currNeighbor.getBestNb().contains(idSeg)){
								mergeSegment(currSegment, currNeighbor); //merge Segments
								currNeighbor.getBestNb().clear();
								currNeighbor.setMergingCost(Double.MAX_VALUE);
								hasMerged = true;
								break;
							}
						}
					}
					currSegment.getBestNb().clear();
					currSegment.setMergingCost(Double.MAX_VALUE);
				}
				idSeg++;
			}
			step++;
		}
		
	}
	
	private static void computeSegmentationNotMutual(){
		MultiSegment currSegment = null;
		MultiSegment currNeighbor = null;
		int auxPixel = -1;
		
		ArrayList <Integer> nbSeg= new ArrayList <Integer>();
		
		boolean hasMerged=true;
		int step=0;
		while (hasMerged && step < _maxSteps){
			hasMerged=false;
			
			int idSeg;
			for (int i=0; i<_visitingOrder.length; i++){
				idSeg=_visitingOrder[i];
				currSegment = _segmentsPtr.get(idSeg);
				
				//If it is a valid segment
				if (idSeg == currSegment.getId()){
					auxPixel = idSeg;

					/* for each outline pixel */
					double minFusion;
					while (auxPixel!=-1)
					{
						if (_segmentsPtr.get(auxPixel).isBorderline()){
							int [] nb = getPixelIdFromNeighbors(auxPixel);
							  
							for (int n=0;n<4;n++)
							{
								if (nb[n] != -1)
								{
									nb[n] = _segmentsPtr.get(nb[n]).getId();
									//if neighbor is not from same segment
									if (nb[n]!= i){
										if (!nbSeg.contains(nb[n])){
											nbSeg.add(nb[n]);
										}
									}
								}
															}
						 }
						//next pixel
						 auxPixel = _segmentsPtr.get(auxPixel).getNextPixelId();
					}
					
					/* calculate heterogeinity factors for each neighbor */
					minFusion =  Double.MAX_VALUE;
					int bestNbId = -1;

					for (int n: nbSeg){
						currNeighbor = _segmentsPtr.get(n);
						double cost = GetCost(currSegment, currNeighbor);
									
						if (cost < _scale){ //already the square
							if (cost < minFusion){
								minFusion = cost;
								bestNbId = n;
							}
							//always merge with lower ID (when it is a draw)
							else if (cost == minFusion && bestNbId < n){
								bestNbId = n;
							}
						}				 
					}
					if (bestNbId != -1){
						currNeighbor = _segmentsPtr.get(bestNbId);
						mergeSegment(currSegment, currNeighbor);
						hasMerged=true;
					}
					nbSeg.clear();
				}
			}
			//resetSegments();
			step++;
			//System.out.println(step);
		}
		
	}

	public static void randomOrder() throws Exception
	{
		//TODO: Put it on random order
		_visitingOrder= new int[_imageW * _imageH];
		for (int i=0; i< _visitingOrder.length; i++){
			_visitingOrder[i]=i;
		}
	}
	
	public static int[] getPixelIdFromNeighbors (int pixelId){    
	    int x = pixelId % _imageW;
		int y = pixelId / _imageW;
		
		int [] neighb =new int[4];		

		neighb[0] = (y>0) ? ((y-1)*_imageW) + x : -1; //north
		neighb[1] = (x>0) ? (y*_imageW) + (x-1) : -1; //west
		neighb[2] = (y<(_imageH-1)) ? ((y+1)*_imageW) + x : -1; //south
		neighb[3] = (x<(_imageW-1)) ? (y*_imageW) + (x+1) : -1; //east

		return neighb;
	}
	
	public static double calcColorStats (MultiSegment obj, MultiSegment neighb)
	{
		double[] mean = new double[_nbands];
		double[] colorSum = new double[_nbands];
		double[] squarePixels = new double[_nbands];
		double[] stddev = new double[_nbands];
		
		double[] color_f = new double [_nbands];
		double color_h=0;
		
		double areaObj, areaNb, areaRes;
		
		areaObj = obj.getArea();
		areaNb =  neighb.getArea();
		areaRes = areaObj+areaNb;
		
		// calculates color factor per band and total
		color_h = 0;
		for (int b = 0; b < _nbands; b++)
		{
			mean[b] = ((obj.getAvg_colorByIndex(b) *areaObj)+(neighb.getAvg_colorByIndex(b)*areaNb))/areaRes;
			squarePixels[b] = (obj.getAvg_color_squareByIndex(b))+(neighb.getAvg_color_squareByIndex(b));	
			colorSum[b] = obj.getColor_sumByIndex(b) + neighb.getColor_sumByIndex(b);	
			stddev[b] = Math.sqrt(Math.abs(squarePixels[b] - 2*mean[b]*colorSum[b] + areaRes*mean[b]*mean[b])/areaRes);
						
			color_f[b] = _wBand[b] * ((areaRes*stddev[b]) - ((areaObj* obj.getStd_colorByIndex(b)) + (areaNb * neighb.getStd_colorByIndex(b))));
			color_h += color_f[b];
		}
		
		return color_h;
	}
	
	public static double CalcSpatialStats (MultiSegment obj, MultiSegment neighb){
		double areaObj, areaNb, areaRes;
		double spatial_h =0, smooth_f=0, compact_f =0;
		double perimObj, perimNb, perimRes;
		double bboxObjLen, bboxNbLen, bboxResLen;
		double [] bboxRes;
		
		areaObj = obj.getArea();
		areaNb =  neighb.getArea();
		areaRes = areaObj+areaNb;
		
		perimObj = obj.getPerimeter();
		perimNb = neighb.getPerimeter();
		
		if (areaRes<4){ /* valid only if pixel neighborhood==4 */
			if (areaRes ==2){
				perimRes=6;
			}
			else {
				perimRes=8;
			}
		}
		else{
			perimRes = calcPerimeter(obj, neighb);
		}
		
		bboxObjLen = obj.getB_boxByIndex(2)*2 +  obj.getB_boxByIndex(3)*2;
		bboxNbLen =  neighb.getB_boxByIndex(2)*2 +  neighb.getB_boxByIndex(3)*2;
		bboxRes = calcBbox(obj, neighb);		
		bboxResLen = bboxRes[2]*2 + bboxRes[3]*2;
		
		/* smoothness factor */
		smooth_f = (areaRes*perimRes/bboxResLen - 
		 (areaObj*perimObj/bboxObjLen + areaNb*perimNb/bboxNbLen));

		/* compactness factor */
		compact_f = (Math.sqrt(areaRes)*perimRes - 
		 (Math.sqrt(areaObj)*perimObj + Math.sqrt(areaNb)*perimNb));

		/* spatial heterogeneity */
		spatial_h = _wCmpt*compact_f + (1-_wCmpt)*smooth_f;


		return spatial_h;
	}
	
	public static double calcPerimeter (MultiSegment obj, MultiSegment neighb){
		double perimTotal;
		int idNb;
		int auxPixel;
		
		perimTotal = obj.getPerimeter() + neighb.getPerimeter();
		//choose segment with smaller perimeter
		if ( obj.getPerimeter() <= neighb.getPerimeter() )
		{
			auxPixel = obj.getId();
			idNb = neighb.getId();
		}
		else
		{
			auxPixel = neighb.getId();
			idNb = obj.getId();
		}
		
		// for each pixel from the smaller perimeter segment
		while ( auxPixel != -1){
			// just for borderline
			if ( _segmentsPtr.get(auxPixel).isBorderline())
			{
				int [] nb = getPixelIdFromNeighbors(auxPixel);

				 //verify the neighbor pixels
				 for (int i = 0; i < 4; i++ )
				 {
					   if ( nb[i] != -1 ) // if it is not image limit 
					   {
						   nb[i] = _segmentsPtr.get(nb[i]).getId();
						   if (idNb == nb[i]){ //if it is part of the bigger
							   perimTotal =-2;  
						   }
					   }
				 }
			}
			auxPixel = _segmentsPtr.get(auxPixel).getNextPixelId();
		}
		return perimTotal;
	}
	
	public static double[] calcBbox (MultiSegment obj, MultiSegment neighb){
			double [] bbox=new double[4];
			double minRowObj, maxRowObj, minColObj, maxColObj;
			double minRowNb, maxRowNb, minColNb, maxColNb;
			double minRowRes, maxRowRes, minColRes, maxColRes;
		 		   
			minRowObj = obj.getB_boxByIndex(0);
			maxRowObj = obj.getB_boxByIndex(0)+obj.getB_boxByIndex(2);
			minColObj = obj.getB_boxByIndex(1);
			maxColObj = obj.getB_boxByIndex(1)+obj.getB_boxByIndex(3);
			
			minRowNb = neighb.getB_boxByIndex(0);
			maxRowNb = neighb.getB_boxByIndex(0)+neighb.getB_boxByIndex(2);
			minColNb = neighb.getB_boxByIndex(1);
			maxColNb = neighb.getB_boxByIndex(1)+neighb.getB_boxByIndex(3);
		 
		   if (minRowObj < minRowNb) minRowRes=minRowObj; else minRowRes=minRowNb;
		   if (minColObj < minColNb) minColRes=minColObj; else minColRes=minColNb;
		   if (maxRowObj > maxRowNb) maxRowRes=maxRowObj; else maxRowRes=maxRowNb;
		   if (maxColObj > maxColNb) maxColRes=maxColObj; else maxColRes=maxColNb;
		 
		   bbox[0] = minRowRes;
		   bbox[1] = minColRes;
		   bbox[2] = maxRowRes-minRowRes;
		   bbox[3] = maxColRes-minColRes;
		   
		   return bbox;
	 }
	
	private static void mergeSegment(MultiSegment obj, MultiSegment neighb){	
		double areaObj, areaNb, areaRes;
				
		areaObj = obj.getArea();
		areaNb =  neighb.getArea();
		areaRes = areaObj+areaNb;
		
		// calculates color factor per band and total
		for (int b = 0; b < _nbands; b++)
		{
			obj.setAvg_colorByIndex(((obj.getAvg_colorByIndex(b) *areaObj)+(neighb.getAvg_colorByIndex(b)*areaNb))/areaRes,b);
			obj.setAvg_color_squareByIndex((obj.getAvg_color_squareByIndex(b))+(neighb.getAvg_color_squareByIndex(b)),b);	
			obj.setColor_sumByIndex((obj.getColor_sumByIndex(b))+(neighb.getColor_sumByIndex(b)),b);	
			obj.setStd_colorByIndex(Math.sqrt(Math.abs(obj.getAvg_color_squareByIndex(b) 
					- 2*obj.getAvg_colorByIndex(b)*obj.getColor_sumByIndex(b) 
					+ areaRes*obj.getAvg_colorByIndex(b)*obj.getAvg_colorByIndex(b))/areaRes),b);
		}
	

		obj.setPerimeter(calcPerimeter(obj, neighb));
		obj.setB_box(calcBbox(obj, neighb));
		
		obj.setArea(areaRes);
		
		resetPixels(obj, neighb);
	}
	
	
	private static void resetPixels(MultiSegment obj, MultiSegment neighb){	
		int  neighborId, segmentId;
		int auxPixel;
	
		neighborId = neighb.getId();
		segmentId = obj.getId();
	
		/* for each pixel of the neighbor segment to be merged */
		auxPixel = neighborId;
		while (auxPixel != -1)
		{
			 /* if it is outline pixel, check if that must be changed */
			 if (_segmentsPtr.get(auxPixel).isBorderline()== true) /* curr_segment->area>6, only valid for pixel neighborhood==4 */
			 {
				 int [] nb = getPixelIdFromNeighbors(auxPixel);
				 /* if pixel is surrounded by pixels of the same segment or of the merged neighbor, 
					it's no inter a border pixel */
				 int i=0;
				 while ((i<4)&&(i>-1)){
					 if (nb[i] == -1){ /* image limit */
						 i=-1; 
					 } else {
						 nb[i] = _segmentsPtr.get(nb[i]).getId();
						 if ((nb[i] != segmentId)&&(nb[i] != neighborId))
					     {
					    	 i=-1;
					     }
					     else
					     {
					    	 i++;
					     }
					 }
				 }
				 if (i!=-1) /* no inter outline pixel */
				 {
					 _segmentsPtr.get(auxPixel).setBorderline(false);
				 }
			 }
			 
			/* changes the value of the pixel in the segment matrix (assign it to the current segment) */
			_segmentsPtr.get(auxPixel).setId(segmentId);
			 
			auxPixel = _segmentsPtr.get(auxPixel).getNextPixelId();
		}


		if (obj.getArea()>6) /* curr_segment->area>6, only valid for pixel neighborhood==4 */
		{
			 /* for each outline pixel of the current segment */
			auxPixel = segmentId;
			 while (auxPixel != -1)
			 {
			   if (_segmentsPtr.get(auxPixel).isBorderline()==true)
			   {
				   int [] nb = getPixelIdFromNeighbors(auxPixel);
					 /* if pixel is surrounded by pixels of the same segment or of the merged neighbor, 
						it's no longer a border pixel */
					 int i=0;
					 while ((i<4)&&(i>-1)){
						 if (nb[i] == -1){ /* image limit */
							 i=-1; 
						 } else {
							 nb[i] = _segmentsPtr.get(nb[i]).getId();
							 if ((nb[i] != segmentId)&&(nb[i] != neighborId))
						     {
						    	 i=-1;
						     }
						     else
						     {
						    	 i++;
						     }
						 }
					     
					 }
					 if (i!=-1) /* no longer outline pixel */
				 {
						 _segmentsPtr.get(auxPixel).setBorderline(false);
					 }
				 }
				 auxPixel = _segmentsPtr.get(auxPixel).getNextPixelId();
			 }
		}
		
		// include pixel list of neighbor in the list of curr_segment
		int last =  obj.getLastPixelId();
		_segmentsPtr.get(last).setNextPixelId(neighborId);
		obj.setLastPixelId(neighb.getLastPixelId());
	}

//	private static void resetSegments(){	
//		int  id;
//		Segment auxSegment;
//
//		/* mark all segments as unused */
//		for (int i=0; i< _visitingOrder.length; i++)
//		{
//			id = _visitingOrder[i];
//			auxSegment = _segmentsPtr.get(id);
//			if (auxSegment.getId() != id){
//				//TODO: Remove segment from VisitngOrder
//				//i--;
//			} else {
//				auxSegment.setUsed(false);
//			}
//			
//			
//		}
//	}
	
	public static double GetCost (MultiSegment segment, MultiSegment nbSegment){
		double cost =0;
		if (_wColor > 0){
			//spectral cost
			cost = calcColorStats(segment, nbSegment) * _wColor;	
		}		
		if ((1- _wColor) > 0)
		{
			//morphological cost
			cost = cost + CalcSpatialStats(segment, nbSegment) * (1-(_wColor));
		}
		return cost;
	}
	
	
	public static int[] GetTileWidthHeight(String url, String img, String id) throws IOException{
		int size[] = {0,0};
		
		//Getting width and height
    	URL worldFile = new URL(url + img + "/" + id + ".meta");
    	
    	URLConnection urlConn  = null;
    	InputStreamReader inStream = null;
    	
    	BufferedReader reader = null;
    	
    	for (int i=0; i < ConnAttempts; i++) {
    		
    		try{
    			
    			urlConn = worldFile.openConnection();
    			
    			urlConn.setReadTimeout(50000);
    			urlConn.setConnectTimeout(5000);
    			urlConn.connect();
    			inStream = new InputStreamReader(urlConn.getInputStream());
    			
    			reader = new BufferedReader(inStream);
    		        
    			String line;
		        int index = 0;
		        while ((line = reader.readLine()) != null) {
		        	if (!line.trim().isEmpty()) {
		        		if (index==1)
		        			size[0] = Integer.parseInt(line);
		        		else if (index==2)
		        			size[1] = Integer.parseInt(line);
		        		index++;
		        	}
		        }
    		}
    		catch (IOException e){
    			continue;
    		}
    		
    		break;
    	}

        reader.close();
        inStream.close();
        
		return size;
	}
}
