/*Copyright 2014 Computer Vision Lab

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package br.puc_rio.ele.lvc.interimage.operators.udf;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.pig.EvalFunc;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.logicalLayer.schema.Schema;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.index.strtree.STRtree;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.io.WKTWriter;

import br.puc_rio.ele.lvc.interimage.common.GeometryParser;
import br.puc_rio.ele.lvc.interimage.common.SpatialIndex;
import br.puc_rio.ele.lvc.interimage.common.Tile;

/**
 * UDF for post processing tile based segmentation, i.e. reducing the artifacts among tile borders.
 * This version considers a re-segmentation of the boundary objects through a recursive call of the UDF
 * @author Patrick Happ, Rodrigo Ferreira
 *
 */

//TODO: Use the adjacent tile with the major intersection area?

public class Post extends EvalFunc<DataBag> {
	
	private static int ConnAttempts = 10; //number of connection attempts before fail

	private final static GeometryParser _geometryParser = new GeometryParser();
	private String _imageUrl = null;
	private String _image = null;
	private double _scale = 0.0;
	private static int _numBands = 0;
	
	private static double _wColor;
	private static double _wCmpt;
	private static double [] _wBand;

	private static WKTWriter _writer;
	
	private static String _gridUrl = null;
	private static STRtree _gridIndex = null;
	private static double _resolution =1;
	
	
	/**Constructor that takes the tiles grid URL*/
	public Post(String imageUrl, String image, String scale, String wColor, String wCmpt, String wBands, String gridUrl, String resolution) {		
		_imageUrl = imageUrl;
		_image = image;
		_scale = Math.pow(Double.parseDouble(scale),2); //scale ^2
		
		_wColor = Double.parseDouble(wColor);
		_wCmpt = Double.parseDouble(wCmpt);
		
		String[] bands = wBands.split(",");
		double sum = 0.0;
		
		_numBands = bands.length;
		_wBand = new double[_numBands];
		for (int i=0; i<_numBands; i++) {
			sum = sum + Double.parseDouble(bands[i]);
		}
		for (int i=0; i<_numBands; i++) {
			_wBand[i] = Double.parseDouble(bands[i]) / sum;
		}
		
		if (!gridUrl.isEmpty())
			_gridUrl = gridUrl;
		
		if (!resolution.isEmpty()){
			_resolution = Double.parseDouble(resolution);
		}
		
		
		_writer = new WKTWriter();
	}
		
	/**
     * Method invoked on every bag during foreach evaluation.
     * @param input tuple<br>
     * first column is assumed to have a bag
     * @exception java.io.IOException
     * @return a bag with the neighborhood assignment
     */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public DataBag exec(Tuple input) throws IOException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss"); 
		System.out.println("LOG: START @ " + sdf.format(System.currentTimeMillis()));
		
		if (input == null || input.size() == 0)
            return null;
		
		try {
			
			DataBag bagIn = BagFactory.getInstance().newDefaultBag(); //input
			for (int i=0; i<input.size(); i++) {
				DataBag auxBag = DataType.toBag(input.get(i));
				for (Tuple elem : auxBag) {
					bagIn.add(elem);
		        }			
			}
			
			//System.out.println("LOG: ALL BAGS @ " + sdf.format(System.currentTimeMillis()));
			//System.out.println("SIZE: " + bagIn.size()  + " @ " + sdf.format(System.currentTimeMillis()));
					 
			DataBag bagOut = BagFactory.getInstance().newDefaultBag(); 	//output
			
			//Get Index of tiles
			if (_gridIndex == null) {
				_gridIndex = new STRtree();
				
				//Creates an index for the grid
		        try {
		        	if (!_gridUrl.isEmpty()) {
		        		
		        		URL url  = new URL(_gridUrl);	     
		        		
		        		URLConnection urlConn  = null;
			        	InputStream buff = null;
			        	ObjectInputStream in = null;
			        	
			        	List<Tile> tiles =null;
			        	
			        	for (int i=0; i < ConnAttempts; i++) {
			        		try{
			        			
			        			urlConn = url.openConnection();
			        			
			        			urlConn.setReadTimeout(50000);
			        			urlConn.setConnectTimeout(5000);
			        			urlConn.connect();
			                    buff = new BufferedInputStream(urlConn.getInputStream());
			                    in =  new ObjectInputStream(buff);
			                    
			                    tiles = (List<Tile>)in.readObject();
			        		}
			        		catch (IOException e){
			        			continue;
			        		}
			        		
			        		break;
			        	}
		        		
		    			    		    					    
					    for (Tile t : tiles) {
					    	Geometry geometry = new WKTReader().read(t.getGeometry());
	    					_gridIndex.insert(geometry.buffer(_resolution*0.5).getEnvelopeInternal(),t);
					    }
					    
					    in.close();
					    buff.close();				        			        
		        	}
		        } catch (Exception e) {
					throw new IOException("Caught exception reading grid file ", e);
				}
			}
			
			//System.out.println("LOG: GRID INDEX COMPLETED @ " + sdf.format(System.currentTimeMillis()));
			
			ArrayList <Tuple> auxObjects = new ArrayList();
			ArrayList <Tuple> borderObjects = new ArrayList();
			
			//**INITIALIZING**
			//Process the "next", postpone the "delayed"
			Iterator it = bagIn.iterator();
				
			while (it.hasNext()) {
	        	Tuple t = (Tuple)it.next();
	        	auxObjects.add(t);
	        }
			
			//TODO:For each tile from objects:
			//get spectral values		
			//TODO: DO THE BAATZ & SCHAPE
	        
	        Map<String,Object> bestProps;
	        double bestCost;
	        Tuple bestTuple;
	        
	        //**COMPUTING**
			boolean hasMerged=true;
			while (hasMerged){
				//System.out.println("LOG: STEP START @ " + sdf.format(System.currentTimeMillis()));
				
				borderObjects = auxObjects;
		        auxObjects = new ArrayList();
		        //System.out.println("LOG: BORDER OBJECTS FILTERED - SIZE:" + borderObjects.size() + " @ " + sdf.format(System.currentTimeMillis()));
		        
		        //Create Index
		        SpatialIndex index = createIndex(borderObjects);
		        //System.out.println("LOG: SPATIALINDEX - SIZE: " + index.size());
		        
				hasMerged=false;
		        for (Tuple t : borderObjects){
		        	bestCost=Double.MAX_VALUE;
		        	bestProps=null;
		        	bestTuple=null;

		        	if (!DataType.toMap(t.get(2)).containsKey("Merged")){
		        		//Get geometry and properties
		        		Geometry geom1 = _geometryParser.parseGeometry(t.get(0));
		        		Map<String,Object> props1 = DataType.toMap(t.get(2));	        		

		        		List<Tuple> listNeighbors = index.query(geom1.getEnvelopeInternal());
		        		//System.out.println("LOG: CHECK NB @ " + sdf.format(System.currentTimeMillis()));
		        		//System.out.println("LOG: NEIGHBORS - SIZE: " + listNeighbors.size());
		        		for (Tuple t2 : listNeighbors) {
			        		Geometry geom2 = _geometryParser.parseGeometry(t2.get(0));
			        		//If it's in fact a neighboring polygon
			        		if (geom2.intersects(geom1)) {
		        				//Get Cost
		        				//TODO: Send a tuple, props?
		        				Map<String,Object> props = new HashMap<String,Object>(props1);
		        				Map<String,Object> props2 = DataType.toMap(t2.get(2));
		        				if ((!props2.containsKey("Processed")) && (!props2.containsKey("Merged"))){
		        					if (!(props1.get("iiuuid")).equals(props2.get("iiuuid"))){
		        						double cost = GetCost(t, t2, props);
			    			        	//If the value is under Threshold then merge polygons
			    			        	if ((Double.compare(cost,_scale)<=0) && (Double.compare(cost,bestCost) < 0)){ //TODO: Choose heuristic
			    			        		bestCost=cost;
			    				        	bestProps=props;
			    				        	bestTuple=t2;
			    				        	//System.out.println("LOG: BEST!");
			    			        	}
		        					}
		        				}
		        			}
			        	}
			        	//System.out.println("LOG: NB CHECKED @ " + sdf.format(System.currentTimeMillis()));
			        	
			        	if (bestTuple != null){	        		
			        		//merge polygons
			        		Geometry geom2 = _geometryParser.parseGeometry(bestTuple.get(0));
			        		geom1 = geom1.union(geom2);
			        		t.set(0, _writer.write(geom1));
			        		t.set(2, bestProps);//TODO: check if it is necessary
			        		
			        		Map<String,Object> props2 = DataType.toMap(bestTuple.get(2));
			        		props2.put("Merged", "true");
			        		bestTuple.set(2,props2);
			        		
			        		hasMerged=true;
			        	}
			        	props1.put("Processed", "true");
			        	auxObjects.add(t);
		        	}
		        }
		        //System.out.println("LOG: STEP END @ " + sdf.format(System.currentTimeMillis()));
			}
			
	        //**FINISHING**
			//System.out.println("LOG: BORDER OBJECTS Processed - SIZE:" + auxObjects.size() + " @ " + sdf.format(System.currentTimeMillis()));
	        for (Tuple t : auxObjects){
	        	
	        		//verify if it will be next/false for the next quadtree
		        	String tileBorder = "false";
	        		Map<String,Object> props = DataType.toMap(t.get(2));
	        		props.put("tileBorder",tileBorder );
	        		if (props.containsKey("Processed")){
	        			props.remove("Processed");
	        		}
	        		bagOut.add(t);
	        		
	        		//System.out.println("LOG: ADD TO OUTPUT @ " + sdf.format(System.currentTimeMillis()));
	        }
	       
	        //System.out.println("LOG: BORDER OBJECTS END - SIZE:" + bagOut.size() + " @ " + sdf.format(System.currentTimeMillis()));
	        
			return bagOut;
			
		} catch (Exception e) {
			throw new IOException("Caught exception processing input row, error - " + e.getMessage());
		}
		
	}
	
	
	public static double GetCost (Tuple t1, Tuple t2, Map<String,Object> props) throws ExecException{
		//TODO: Use or not an auxiliar Tuple?
		double cost =0;
		if (_wColor > 0){
			//spectral cost
			cost = calcColorStats(t1, t2, props) * _wColor;	
		}		
		if ((1- _wColor) > 0)
		{
			//morphological cost
			//cost = cost + CalcSpatialStats(t1, t2) * (1-(_wColor)); //TODO: GET RESOLUTION
		}
		return cost;
	}
	
	public static double calcColorStats (Tuple t1, Tuple t2, Map<String,Object> props) throws ExecException
	{
		double mean;
		double colorSum;
		double squarePixels;
		double stddev;
		
		double color_f;
		double color_h=0;
				
		double areaObj, areaNb, areaRes;
		
		// calculates color factor per band and total
		color_h = 0;
		String[] name={"Mean_", "Square_", "Sum_", "Std_"};
		double [] valObj =  new double [4];
		double [] valNb =  new double [4];
		
		Map<String,Object> props1 = DataType.toMap(t1.get(2));
		Map<String,Object> props2 = DataType.toMap(t2.get(2));
		
		//areaObj = _geometryParser.parseGeometry(t1.get(0)).getArea();
		//areaNb =  _geometryParser.parseGeometry(t2.get(0)).getArea();
		areaObj = DataType.toDouble(props1.get("Area"));
		areaNb =  DataType.toDouble(props2.get("Area"));
		areaRes = areaObj+areaNb;
		
		for (int b = 0; b < _numBands; b++)
		{
			for (int i=0; i<4; i++){
				valObj[i]=DataType.toDouble(props1.get(name[i].concat(String.valueOf(b))));
				valNb[i]=DataType.toDouble(props2.get(name[i].concat(String.valueOf(b))));
			}
			
			mean = ((valObj[0] *areaObj)+(valNb[0]*areaNb)) / areaRes;
			squarePixels = valObj[1]  + valNb[1];	
			colorSum = valObj[2] + valNb[2];	
			stddev = Math.sqrt(Math.abs(squarePixels - 2*mean*colorSum + areaRes*mean*mean)/areaRes);
			
			/*System.out.println("Dev");
			System.out.println(stddev);
			System.out.println(valObj[3]);
			System.out.println(valNb[3]);
			System.out.println(areaRes);
			System.out.println(areaObj);
			System.out.println(areaNb);*/
						
			color_f = _wBand[b] * ((areaRes*stddev) - ((areaObj* valObj[3]) + (areaNb * valNb[3])));
			color_h += color_f;
			
			props.put(name[0].concat(String.valueOf(b)),mean);
			props.put(name[1].concat(String.valueOf(b)),squarePixels);
			props.put(name[2].concat(String.valueOf(b)),colorSum);
			props.put(name[3].concat(String.valueOf(b)),stddev);
		}
		props.put("Area",areaRes);
				
		return color_h;
	}
	
	public static double CalcSpatialStats (Tuple t1, Tuple t2) throws ExecException{
		double areaObj, areaNb, areaRes;
		double spatial_h =0, smooth_f=0, compact_f =0;
		double perimObj, perimNb, perimRes;
		double bboxObjLen, bboxNbLen, bboxResLen;
		
		Geometry geom1 = _geometryParser.parseGeometry(t1.get(0));
		Geometry geom2 =  _geometryParser.parseGeometry(t2.get(0));
		
		Geometry geomRes = geom1.union(geom2);
		
		areaObj = geom1.getArea();
		areaNb =  geom2.getArea();
		areaRes = areaObj+areaNb;
		
		perimObj = geom1.getLength();
		perimNb = geom2.getLength();
		perimRes = geomRes.getLength();

		bboxObjLen = geom1.getEnvelope().getLength();
		bboxNbLen =  geom2.getEnvelope().getLength();
		bboxResLen = geomRes.getEnvelope().getLength();
		
		/* smoothness factor */
		smooth_f = (areaRes * perimRes/bboxResLen - 
		 (areaObj*perimObj/bboxObjLen + areaNb*perimNb/bboxNbLen));

		/* compactness factor */
		compact_f = (Math.sqrt(areaRes)*perimRes - 
		 (Math.sqrt(areaObj)*perimObj + Math.sqrt(areaNb)*perimNb));

		/* spatial heterogeneity */
		spatial_h = _wCmpt*compact_f + (1-_wCmpt)*smooth_f;

		return spatial_h;
	}
	
	/**This method creates an STR-Tree index for the input list and returns it.*/
	private SpatialIndex createIndex(ArrayList<Tuple> list, String id) {
		
		SpatialIndex index = null;
		
		try {
		
			index = new SpatialIndex();
					
			for (Tuple t:list){
				if (!DataType.toMap(t.get(2)).containsKey("Merged")){
					if (DataType.toString(DataType.toMap(t.get(2)).get("iiuuid")) != id){
						Geometry geometry = _geometryParser.parseGeometry(t.get(0));
		        		//index.insert(geometry.getEnvelopeInternal(),t);
						index.insert(geometry.getEnvelopeInternal(),t);
					}
				}
			}
		} catch (Exception e) {
			System.err.println("Failed to index bag; error - " + e.getMessage());
			return null;
		}
				
		return index;
	}
	
	/**This method creates an STR-Tree index for the input list and returns it.*/
	private SpatialIndex createIndex(ArrayList<Tuple> list) {
		
		SpatialIndex index = null;
		
		try {
		
			index = new SpatialIndex();
					
			for (Tuple t:list){
				if (!DataType.toMap(t.get(2)).containsKey("Merged")){
					Geometry geometry = _geometryParser.parseGeometry(t.get(0));
	        		//index.insert(geometry.getEnvelopeInternal(),t);
					index.insert(geometry.getEnvelopeInternal(),t);
					Map<String,Object> props1 = DataType.toMap(t.get(2));
					props1.remove("Processed");
				}
			}
		} catch (Exception e) {
			System.err.println("Failed to index bag; error - " + e.getMessage());
			return null;
		}
				
		return index;
	}
	
	
	
	@Override
    public Schema outputSchema(Schema input) {
        
		try {

			List<Schema.FieldSchema> list = new ArrayList<Schema.FieldSchema>();
			list.add(new Schema.FieldSchema(null, DataType.CHARARRAY));
			list.add(new Schema.FieldSchema(null, DataType.MAP));
			list.add(new Schema.FieldSchema(null, DataType.MAP));
			
			Schema tupleSchema = new Schema(list);
			
			Schema.FieldSchema ts = new Schema.FieldSchema(null, tupleSchema, DataType.TUPLE);
			
			Schema bagSchema = new Schema(ts);
			
			Schema.FieldSchema bs = new Schema.FieldSchema(null, bagSchema, DataType.BAG);
			
			return new Schema(bs);

		} catch (Exception e) {
			return null;
		}
		
    }
	
}
