/*Copyright 2014 Computer Vision Lab

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package br.puc_rio.ele.lvc.interimage.operators.udf;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.stream.ImageInputStream;

import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.img.Img;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.roi.BinaryMaskRegionOfInterest;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.real.DoubleType;

import org.apache.pig.EvalFunc;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.schema.Schema;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.prep.PreparedGeometry;
import com.vividsolutions.jts.geom.prep.PreparedGeometryFactory;
import com.vividsolutions.jts.index.strtree.STRtree;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.io.WKTWriter;

import br.puc_rio.ele.lvc.interimage.common.GeometryParser;
import br.puc_rio.ele.lvc.interimage.common.SpatialIndex;
import br.puc_rio.ele.lvc.interimage.common.Tile;
import br.puc_rio.ele.lvc.interimage.common.UUID;
import br.puc_rio.ele.lvc.interimage.data.Image;
import br.puc_rio.ele.lvc.interimage.data.imageioimpl.plugins.tiff.TIFFImageReader;
import br.puc_rio.ele.lvc.interimage.operators.MultiSegment;

/**
 * UDF for post processing tile based segmentation, i.e. reducing the artifacts among tile borders.
 * This version considers a re-segmentation of the boundary objects through a recursive call of the UDF
 * and includes the dissolution of the segment into pixels for each call
 * @author Patrick Happ, Rodrigo Ferreira
 *
 */

//TODO: How to select the new tileID for the new segments? Choose one for all?

public class Dissolve extends EvalFunc<DataBag> {
	
	private static int ConnAttempts = 10; //number of connection attempts before fail
	
	private final static GeometryParser _geometryParser = new GeometryParser();
	private String _imageUrl = null;
	private String _image = null;
	private static double _scale = 0.0;
	private static int _numBands = 0;
	
	private static double _wColor;
	private static double _wCmpt;
	private static double [] _wBand;

	private static WKTWriter _writer;
	
	private static String _gridUrl = null;
	private static STRtree _gridIndex = null;
	private static double _resolution =1;
	private static int _level =0;
	private static int _tileSize = 512; //tile size 
	
	double[] imageFullTileGeoBox = new double[4];
	
	static int numTiles = 0;
	static int _numTilesX = 0;
	static int _numTilesY = 0;
	static Map<Integer, MultiSegment> _segmentsPtr2;
	static int _nBands=0;
	private static int _maxSteps=1000;

	
	
	/**Constructor that takes the tiles grid URL*/
	public Dissolve(String imageUrl, String image, String scale, String wColor, String wCmpt, String wBands, String gridUrl, String resolution, String level, String tileSize) {		
		_imageUrl = imageUrl;
		_image = image;
		_scale = Math.pow(Double.parseDouble(scale),2); //scale ^2
		
		_wColor = Double.parseDouble(wColor);
		_wCmpt = Double.parseDouble(wCmpt);
		
		String[] bands = wBands.split(",");
		double sum = 0.0;
		
		_numBands = bands.length;
		_wBand = new double[_numBands];
		for (int i=0; i<_numBands; i++) {
			sum = sum + Double.parseDouble(bands[i]);
		}
		for (int i=0; i<_numBands; i++) {
			_wBand[i] = Double.parseDouble(bands[i]) / sum;
		}
		
		if (!gridUrl.isEmpty())
			_gridUrl = gridUrl;

		if (!resolution.isEmpty()){
			_resolution = Double.parseDouble(resolution);
		}
		
		if (!level.isEmpty()){
			_level = Integer.parseInt(level);
		}
		
		//Check the _tileSize
				if (!tileSize.isEmpty())
					_tileSize = Integer.parseInt(tileSize);
		
		_writer = new WKTWriter();
	}
		
	/**
     * Method invoked on every bag during foreach evaluation.
     * @param input tuple<br>
     * first column is assumed to have a bag
     * @exception java.io.IOException
     * @return a bag with the neighborhood assignment
     */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public DataBag exec(Tuple input) throws IOException {
				
		if (input == null || input.size() == 0)
            return null;
		
		try {
			
			//System.out.println("STARTING");
						
			DataBag bagIn = BagFactory.getInstance().newDefaultBag(); //input
			for (int i=0; i<input.size(); i++) {
				DataBag auxBag = DataType.toBag(input.get(i));
				for (Tuple elem : auxBag) {
					//if (!elem.isNull())
						bagIn.add(elem);
		        }			
			}

			//System.out.println("SIZE: " + bagIn.size());
			DataBag bagOut = BagFactory.getInstance().newDefaultBag(); 	//output

			
			//Get Index of tiles
			if (_gridIndex == null) {
				
				//TODO: Get values from the whole image (all tiles)
				imageFullTileGeoBox[0] = Double.MAX_VALUE;//Double.parseDouble(line1);
				imageFullTileGeoBox[1] = Double.MAX_VALUE;//Double.parseDouble(line1);
				imageFullTileGeoBox[2] = Double.MIN_VALUE;//Double.parseDouble(line1);
				imageFullTileGeoBox[3] = Double.MIN_VALUE;//Double.parseDouble(line1);
				
				_gridIndex = new STRtree();
				
				//Creates an index for the grid
		        try {
		        	if (!_gridUrl.isEmpty()) {
		        		
		        		URL url  = new URL(_gridUrl);	  
		        		
		        		URLConnection urlConn  = null;
			        	InputStream buff = null;
			        	ObjectInputStream in = null;
			        	
			        	List<Tile> tiles =null;
			        	
			        	for (int i=0; i < ConnAttempts; i++) {
			        		try{
			        			
			        			urlConn = url.openConnection();
			        			
			        			urlConn.setReadTimeout(50000);
			        			urlConn.setConnectTimeout(5000);
			        			urlConn.connect();
			                    buff = new BufferedInputStream(urlConn.getInputStream());
			                    in =  new ObjectInputStream(buff);
			                    
			                    tiles = (List<Tile>)in.readObject();
			        		}
			        		catch (IOException e){
			        			continue;
			        		}
			        		
			        		break;
			        	}
		        		
		    		    
		    		    		    		    
					    for (Tile t : tiles) {
					    	Geometry geometry = new WKTReader().read(t.getGeometry());
	    					_gridIndex.insert(geometry.buffer(_resolution*0.5).getEnvelopeInternal(),t);
	    					
	    					Envelope env = geometry.getEnvelopeInternal();
	    					double val = env.getMaxX();
	    			    	if (val > imageFullTileGeoBox[2]){
	    			    		imageFullTileGeoBox[2] = val;
	    			    		_numTilesX = (int) t.getId(); 
	    			    	}
	    			    	
	    			    	val = env.getMinX();
	    			    	if (val < imageFullTileGeoBox[0]){
	    			    		imageFullTileGeoBox[0] = val;
	    			    	}
	    			    	val = env.getMaxY();
	    			    	if (val > imageFullTileGeoBox[3]){
	    			    		imageFullTileGeoBox[3] = val;
	    			    	}
	    			    	val = env.getMinY();
	    			    	if (val < imageFullTileGeoBox[1]){
	    			    		imageFullTileGeoBox[1] = val;
	    			    	}
	    			    	
					    }
					    
					    
					    numTiles = tiles.size();
					    if (_numTilesX>0)
					    	_numTilesY = numTiles/_numTilesX;
					    
					    //System.out.println("NUM TILES X: " + _numTilesX);
					    //System.out.println("NUM TILES Y: " + _numTilesY);
					    //System.out.println("NUM TILE: " + numTiles);
					    
					    in.close();
					    buff.close();				        			        
		        	}
		        } catch (Exception e) {
					throw new IOException("Caught exception reading grid file ", e);
				}
			}
			
			//System.out.println("Before While");
			
			Map <Tile, List<Geometry>> objectsByTile = new HashMap<Tile, List<Geometry>>();
			
			//**INITIALIZING**
			//Process the "next", postpone the "delayed"
			String tileStr = null;
			String crsStr = null;
			Map<String,String> dataMap = null; // TODO: Is it necessary?
			
			Iterator it = bagIn.iterator();
			//int count0 =0;
			//int count1 =0;
				
			/*while (it.hasNext()) {
	        	Tuple t = (Tuple)it.next();
	        	//Get Tile border type
	        	String tileBorder = DataType.toString(DataType.toMap(t.get(2)).get("tileBorder"));
	        	String currTileId = DataType.toString(DataType.toMap(t.get(2)).get("tile")); //TODO: can do this just once
	        	if (tileBorder.equals("delayed")){
	        		//verify if it will be next/delayed for the next quadtree - test with tile
	        		Geometry geom1 = _geometryParser.parseGeometry(t.get(0));
	        		List<Tile> tiles = _gridIndex.query(geom1.getEnvelopeInternal());
	        		for (Tile itTile : tiles) {
						String tileId = itTile.getCode();
						//check the next level,but not the same tile
						if (!currTileId.equals(tileId)){
							if (currTileId.substring(0,currTileId.length()-(_level+1)).equals(tileId.substring(0,tileId.length()-(_level+1)))){
								tileBorder= "next";
								break;
							}
						}
					}
	        		Map<String,Object> props = DataType.toMap(t.get(2));
	        		props.put("tileBorder",tileBorder );
	        		bagOut.add(t);
	        	} else {
	        		//For each tile it intersects
	        		Geometry geom1 = _geometryParser.parseGeometry(t.get(0));
	        		List<Tile> tiles = _gridIndex.query(geom1.getEnvelopeInternal());
	        		for (Tile itTile : tiles) {
	        			//add the polygon in tile's list 	
	        			List<Geometry> list = objectsByTile.get(itTile);
	        			if (list == null){
	        				list = new  ArrayList<Geometry>();
	        			}
	        			list.add(geom1);
	        			if (tileStr == null){ //TODO: Is it the best way?
	     		    	   tileStr = currTileId;
	     		    	   crsStr = DataType.toString(DataType.toMap(t.get(2)).get("crs"));
	     		    	   dataMap = (Map<String,String>)t.get(1);
	     		       }
	        			objectsByTile.put(itTile, list);
	        			//count1++;
	        		}
	        		//count0++;
	        	}
	        }*/
			
			while (it.hasNext()){
				Tuple t = (Tuple)it.next();
				
				//Add to list
				//For each tile it intersects
        		Geometry geom1 = _geometryParser.parseGeometry(t.get(0));
        		List<Tile> tiles = _gridIndex.query(geom1.getEnvelopeInternal());
        		for (Tile itTile : tiles) {
        			//add the polygon in tile's list 	
        			List<Geometry> list = objectsByTile.get(itTile);
        			if (list == null){
        				list = new  ArrayList<Geometry>();
        			}
        			list.add(geom1);
        			if (crsStr == null){ //TODO: Is it the best way?
     		    	   crsStr = DataType.toString(DataType.toMap(t.get(2)).get("crs"));
     		    	   tileStr =  DataType.toString(DataType.toMap(t.get(2)).get("tile")); //TODO: can do this just once
     		    	   dataMap = (Map<String,String>)t.get(1);
     		       }
        			objectsByTile.put(itTile, list);
        		}
			}
			
			
			//System.out.println("Original Input objects: " + count0);
			//System.out.println("Input objects with duplicates: " + count1);
			_segmentsPtr2 = new HashMap <Integer, MultiSegment>();
			
			//For each tile from objects
			//***GET THE PART OF THE IMAGE --> Vector to raster
			for (Map.Entry<Tile, List<Geometry>> entry : objectsByTile.entrySet())
			{
			    //get the image
				Tile tile = entry.getKey();
				Geometry tileGeom = new WKTReader().read(tile.getGeometry());
				
				//Read Input Image
				String imageFile = _imageUrl + _image + "/" + tile.getCode() + ".tif";
				URL worldFile1 = new URL(imageFile);
				
				URLConnection urlConn = null;
		    	InputStream stream = null;
		    	ImageInputStream in = null;
		    	BufferedImage buff = null;
		    	
		    	TIFFImageReader reader = null;
		    	ImageReadParam param = null;
		    	
		    	for (int i=0; i < ConnAttempts; i++) {
		    		
		    		try{
		    			
		    			urlConn = worldFile1.openConnection();
		    			
		    			urlConn.setReadTimeout(50000);
		    			urlConn.setConnectTimeout(5000);
		    			urlConn.connect();
		    			
		    			stream = new BufferedInputStream(urlConn.getInputStream());
		    			in = ImageIO.createImageInputStream(stream);
		    			
		    			if (in == null)
				        	throw new Exception("Could not create input stream: " + imageFile.toString());
				        
				        reader = new TIFFImageReader(null);
				        reader.setInput(in);
						
				        param = reader.getDefaultReadParam();       
				        buff = reader.read(0,param);
		    		}
		    		catch (IOException e){
		    			continue;
		    		}
		    		
		    		break;
		    	}
				
		        if (buff == null)
					throw new Exception("Could not instantiate tile image: " + imageFile.toString());
				
		        //Get Image Tile coordinates
		        double[] tileGeoBox = new double[4];
		        URL worldFile2 = new URL(_imageUrl + _image + "/" + tile.getCode() + ".meta");
				
				URLConnection urlConn2  = null;
	        	InputStreamReader inStream2 = null;
	        	
	        	BufferedReader reader2 = null;
	        	
	        	for (int i=0; i < ConnAttempts; i++) {
	        		
	        		try{
	        			
	        			urlConn2 = worldFile2.openConnection();
	        			
	        			urlConn2.setReadTimeout(50000);
	        			urlConn2.setConnectTimeout(5000);
	        			urlConn2.connect();
	        			inStream2 = new InputStreamReader(urlConn2.getInputStream());
	        			
	        			reader2 = new BufferedReader(inStream2);
	        			
	        		    String line1;
	    		        int index1 = 0;
	    		        while ((line1 = reader2.readLine()) != null) {
	    		        	if (!line1.trim().isEmpty()) {
	    		        		if (index1==3)
	    		        			tileGeoBox[0] = Double.parseDouble(line1);
	    		        		else if (index1==4)
	    		        			tileGeoBox[1] = Double.parseDouble(line1);
	    		        		else if (index1==5)
	    		        			tileGeoBox[2] = Double.parseDouble(line1);
	    		        		else if (index1==6)
	    		        			tileGeoBox[3] = Double.parseDouble(line1);
	    			        	index1++;
	    		        	}
	    		        }
	        			
	        		}
	        		catch (IOException e){
	        			continue;
	        		}
	        		
	        		break;
	        	}
				

		        //does not work properly cause need the image tile
		        //double [] tileGeoBox = {tileGeom.getEnvelopeInternal().getMinX(),tileGeom.getEnvelopeInternal().getMinY(),tileGeom.getEnvelopeInternal().getMaxX(),tileGeom.getEnvelopeInternal().getMaxY()};
		        
		        List<Geometry> list = new ArrayList<Geometry>();
		        GeometryFactory fact = new GeometryFactory();
		        Geometry geoFinal=null;
		       //Merge all polygons instead visit each one 
		       //TODO: Verify if it is really faster
		       try{
			       for (Geometry geometry : entry.getValue()){
						if (!tileGeom.intersects(geometry)) //if not intersects, something is strange, skip
							continue;
					
						//Gets the part of the polygon inside the tile
						Geometry geom = tileGeom.intersection(geometry);
						if (geom.getArea()==0) // If Area is zero, it do not intersect correctly
							continue;
						
						//Converts from vector to pixel
						int[] bBox = Image.imgBBox(new double[] {geom.getEnvelopeInternal().getMinX(), geom.getEnvelopeInternal().getMinY(), geom.getEnvelopeInternal().getMaxX(), geom.getEnvelopeInternal().getMaxY()},tileGeoBox, new int[] {buff.getWidth(), buff.getHeight()});
						//Test if pixels are part of the tile
						if ((bBox[0] < 0) || (bBox[1] < 0) || (bBox[2] < 0) || (bBox[3] < 0)
								|| (bBox[0] >= buff.getWidth()) || (bBox[1] >= buff.getHeight()) || (bBox[2] >= buff.getWidth()) || (bBox[3] >= buff.getHeight()))
									continue;
						
						list.add(geom);
					}
		       
			       	if (list.size()<1)
						continue;
			       	
			       	Geometry[] geoms = new Geometry[list.size()];
		        	int index = 0;
		        	for (Geometry geom : list) {
		        		geoms[index] = geom;
		        		index++;
		        	}
		        	//System.out.println("ANTES DO UNION");
					geoFinal = fact.createGeometryCollection(geoms).buffer(0);
					//System.out.println("DEPOIS DO UNION");
				
		       	}catch (Exception e) {
					throw new IOException("Caught exception generating Tile Geometry " + e.getMessage());
				}  

		        int[] bBox = Image.imgBBox(new double[] {geoFinal.getEnvelopeInternal().getMinX(), geoFinal.getEnvelopeInternal().getMinY(), geoFinal.getEnvelopeInternal().getMaxX(), geoFinal.getEnvelopeInternal().getMaxY()},tileGeoBox, new int[] {buff.getWidth(), buff.getHeight()});
				Img<BitType> mask = null;
				double[] geoBBox = Image.geoBBox(bBox, tileGeoBox, new int[] {buff.getWidth(), buff.getHeight()});
				int width = bBox[2]-bBox[0]+1;
				int height = bBox[1]-bBox[3]+1;
				
				double resX = (tileGeoBox[2]-tileGeoBox[0])/buff.getWidth();
				double resY = (tileGeoBox[1]-tileGeoBox[3])/buff.getHeight(); 
				
				//Calculate X and Y offset of the mask/cursor
				//Discover the distance in pixels from the mask to original image tile
				int offMaskX = (int)Math.round(Math.abs((geoBBox[0]-tileGeoBox[0])/resX));
				int offMaskY = (int)Math.round(Math.abs((geoBBox[3]-tileGeoBox[3])/resY));
		       
				//Creates mask
				mask = new ArrayImgFactory<BitType>().create(new long[] {width, height} , new BitType());
				Cursor<BitType> c = mask.cursor();
				
				//int count = 0;
				geoFinal = geoFinal.buffer(0);
				PreparedGeometry prep = PreparedGeometryFactory.prepare(geoFinal);
				
				int[] pos = new int[2];
				while(c.hasNext()) {
					BitType t = c.next();
					c.localize(pos);
					
					int x = pos[0];
					int y = pos[1];
					
					double centerPixelX = geoBBox[0] + (x*resX) + (resX/2);
					double centerPixelY = geoBBox[3] + (y*resY) + (resY/2);
					
					Point point = new GeometryFactory().createPoint(new Coordinate(centerPixelX, centerPixelY));
					
					boolean b = false;
					if (prep.covers(point))
						b = true;
					
					//if (b)
					//	count++;
					
					t.set(b);
				}
				
				WritableRaster raster = buff.getRaster();
				int bands = raster.getNumBands();
				if (_nBands == 0){
					_nBands = bands;
				} else if (_nBands != bands){
					throw new Exception("Image bands differs from tile to tile");
				}
				
				//Creates image
				Object[] imgs = new Object[bands];
				Object[] cursors = new Object[bands];
								
				for (int b=0; b<bands; b++) {
					Img<DoubleType> img = new ArrayImgFactory<DoubleType>().create(new long[] {width, height}, new DoubleType());
					imgs[b] = img;
					cursors[b] = img.randomAccess();
				}
				//get values for each band
				for (int b=0; b<bands; b++) {
					RandomAccess<DoubleType> ra = (RandomAccess<DoubleType>)cursors[b];
					for (int j=bBox[3]; j<=bBox[1]; j++) {
						for (int i=bBox[0]; i<=bBox[2]; i++) {
								double value = raster.getSampleDouble(i, j, b);
								
								ra.setPosition(i-bBox[0],0);
								ra.setPosition(j-bBox[3],1);
								
								DoubleType pixelValue = ra.get();
								pixelValue.set(value);
						}
					}
				}
				
				Cursor<DoubleType>[] cursorsBand = new Cursor[bands];
				//Creates masked cursor
				BinaryMaskRegionOfInterest<BitType, Img<BitType>> x = new BinaryMaskRegionOfInterest<BitType, Img<BitType>>(mask);					
				
				for (int b=0; b<bands; b++) {
					cursorsBand[b] = x.getIterableIntervalOverROI(((Img<DoubleType>)imgs[b])).cursor();
				}
				
				MultiSegment auxSegment;
				int id=0;
				//fill segmentation structure
				while (cursorsBand[0].hasNext() )
		        {
					int offX=0; //offset for image tiles on the edge
					int offY=0; //offset for image tiles on the edge
					int corr=0; //correct difference from the order
					
					for (int b=0; b<bands; b++) {
						cursorsBand[b].fwd();
					}
					
		            //final DoubleType t = cursorsBand[0].get();
		            cursorsBand[0].localize(pos);
		            
		            //Get TILE coordinates in order to calculate pixel ID
		            int TileID = (int) tile.getId()-1; //Starts from 1, so decreases 1 to begin at 0
		            
		            //FIRST -> Adjust position & Verify if the buffer is the same size of tile
		            if ((TileID % _numTilesX)==0){
		            	corr=-1;
		            	if (buff.getWidth() < _tileSize){
			            	//TODO: Correct position
			            	if ((TileID % _numTilesX)==0){
			            		offX=(int) (_tileSize - buff.getWidth());
			            	}		            	
			            }
		            }
		            if (buff.getHeight() < _tileSize){
		            	//TODO: Correct position.
		            	if ((numTiles-TileID) < (_numTilesX+1)) {
		            		offY=(int) (_tileSize - buff.getHeight());
		            	}
		            }
		            //MAP the current index to the whole image index
		            //NORMAL
		            /*int part1 = ((TileID % NumTilesX) *_tileSize) + pos[0]; //X Tile Position
		            int part2 = ((TileID / NumTilesX) * NumTilesX * (_tileSize*_tileSize)); // Y Tile Post     
		            int part3 = NumTilesX * _tileSize * pos[1]; // Y internal position*/
		            
		            //Different reading order
		            int part1 = ((TileID % _numTilesX) *_tileSize) + (pos[0] + offMaskX + offX); //X Tile Position
		            int part2 = ((((numTiles-TileID) / _numTilesX)+corr) * _numTilesX * (_tileSize*_tileSize)); // Y Tile Post     
		            int part3 = _numTilesX * _tileSize * (pos[1] + offMaskY + offY); // Y internal position
		            id = part1 + part2 + part3;
		            //id = pos[0] + _imageW*pos[1]; //This considers only the image tile
		           
		            auxSegment = MultiSegment.create(id);
	          		_segmentsPtr2.put(id, auxSegment);
	          		
	          		auxSegment.setB_boxByIndex((double)pos[0], 0);
					auxSegment.setB_boxByIndex((double)pos[1], 1);

	          		auxSegment.createSpectral(bands);
	          		double val;
					for(int b = 0; b < bands; b++)
					{
						val = cursorsBand[b].get().get();
						auxSegment.setAvg_colorByIndex(val, b);
						auxSegment.setStd_colorByIndex(0, b);
						auxSegment.setAvg_color_squareByIndex(val*val, b);
						auxSegment.setColor_sumByIndex(val, b);
					}
					
					//Set Pixel
					auxSegment.setBorderline(true);
					auxSegment.setLastPixelId(id);
					auxSegment.setNextPixelId(-1);
		        }
			}
			
			//System.out.println("Initial Segments: " + _segmentsPtr2.size());
			//System.out.println("BEFORE SEGMENTATION");
			
			computeSegmentation2(); //do the segmentation (Multiresoltion algorithm)
			//TODO: Include non mutual algorithm
			
			//System.out.println("AFTER SEGMENTATION");
			
			//ArrayList <Tuple> auxObjects = new ArrayList();
	        //Write to Vector Again
			
			try{
	    		GeometryFactory fact = new GeometryFactory();
		        WKTWriter writer = new WKTWriter();
		        UUID uuid = new UUID(null);
		        
		        List<Geometry> list = new ArrayList<Geometry>();
		        MultiSegment auxSegment;
	    		int idSeg=0;
	    		int width = _numTilesX*_tileSize;
	    		int height = _numTilesY*_tileSize;
	    		//int count=0;
	    		for  (Map.Entry<Integer, MultiSegment> entry : _segmentsPtr2.entrySet()){	
	       			auxSegment = entry.getValue();
	       			idSeg = entry.getKey();
	       			
	        		if (auxSegment.getId() == idSeg){   
	
	        			int auxPixel = idSeg;
				    	if (auxPixel != -1){
				    					    	
					    	while (auxPixel != -1) {
	
					    		double CoordX, CoordY;
					    		int x = auxPixel % width;
					    		int y = auxPixel / width;
								 
			      				Coordinate [] linePoints = new Coordinate[5];
								//left top corner
			      				CoordX = Image.imgToGeoX(x - 0.5, width, imageFullTileGeoBox);
			      				CoordY = Image.imgToGeoY(y - 0.5 ,height, imageFullTileGeoBox);
			      				linePoints[0]= new Coordinate(CoordX,CoordY);
			      				linePoints[4]= new Coordinate(CoordX,CoordY); //close the ring
				                //right top corner
			      				CoordX = Image.imgToGeoX(x + 0.5, width,imageFullTileGeoBox);
			      				CoordY = Image.imgToGeoY(y - 0.5,height,imageFullTileGeoBox);
			      				linePoints[1] = new Coordinate(CoordX,CoordY);
								//right bottom corner
								CoordX = Image.imgToGeoX(x + 0.5,width,imageFullTileGeoBox);
								CoordY = Image.imgToGeoY(y + 0.5,height,imageFullTileGeoBox);
								linePoints[2] = new Coordinate(CoordX,CoordY);
								//right bottom corner
								CoordX = Image.imgToGeoX(x - 0.5,width,imageFullTileGeoBox);
								CoordY = Image.imgToGeoY(y + 0.5,height,imageFullTileGeoBox);
								linePoints[3] = new Coordinate(CoordX,CoordY);
								 
								LinearRing shell = fact.createLinearRing(linePoints);		          			
			          			//Geometry poly = new Polygon(shell, null, fact);
								Geometry poly = fact.createPolygon(shell, null);
								 
			          			list.add(poly);
			          			
			          			auxPixel = _segmentsPtr2.get(auxPixel).getNextPixelId();
			          			
					        }
					    	Geometry[] geoms = new Geometry[list.size()];
				        	
				        	int index = 0;
				        	for (Geometry geom : list) {
				        		geoms[index] = geom;
				        		index++;
				        	}
				        	
				        	//TODO: union seems slightly faster than buffer(0)
				        	//TODO: Passing the factory as a parameter here created a huge performance issue
				        	//For now, calling the method from the factory, much faster
				        	//Geometry union = new GeometryCollection(geoms, fact).buffer(0);
				        	Geometry union = fact.createGeometryCollection(geoms).buffer(0);
				        	
				        	Tuple t = TupleFactory.getInstance().newTuple(3);
				        	
				        	Map<String,Object> props = new HashMap<String,Object>();
			        		String id = uuid.random();	
			        		props.put("iiuuid", id);
			        		//TODO: Fill the properties? TILE, CRS, What else?
							props.put("crs", crsStr);
							props.put("class", "None");
			        		
			        		//TODO: Delete later, it is just for experiments
							String[] name={"Mean_", "Square_", "Sum_", "Std_"};
							for (int i=0; i<_wBand.length;i++){
								props.put(name[0].concat(String.valueOf(i)),auxSegment.getAvg_colorByIndex(i));
								props.put(name[1].concat(String.valueOf(i)),auxSegment.getAvg_color_squareByIndex(i));
								props.put(name[2].concat(String.valueOf(i)),auxSegment.getColor_sumByIndex(i));
								props.put(name[3].concat(String.valueOf(i)),auxSegment.getStd_colorByIndex(i));
							}
							props.put("Area",auxSegment.getArea());
	
							double myArea=0;
							int tileBorder=Integer.MAX_VALUE; //keep track of boundary segments (MAX=internal; n=frame level)
							int len = tileStr.length();
							List<Tile> tiles = _gridIndex.query(union.getEnvelopeInternal());
							//discover segment tileID
							for (Tile itTile : tiles) {
								Geometry tileGeom = new WKTReader().read(itTile.getGeometry());
								if (tileGeom.intersects(union)){ //if not intersects, something is strange, skip
									Geometry geom = tileGeom.intersection(union);
									double auxArea=geom.getArea();
									if (auxArea>myArea){ // If Area is zero, it do not intersect correctly
										myArea = auxArea;
										tileStr = itTile.getCode();
									}
								}
							}
							//verify borders	
							for (Tile itTile : tiles) {
								String tileId = itTile.getCode();
								if (!(tileStr.substring(0,len-(_level+1)).equals(tileId.substring(0,len-(_level+1))))){
									for (int i=_level+1; i < len ;i++){
										if (tileStr.substring(0,len-i).equals(tileId.substring(0,len-i))){
											//if it is in the same quadtree level
											if (i < tileBorder)
												tileBorder = i;
											//System.out.println("NEXT");
											break;
										}
									}
								}
							}
							if (tileBorder == Integer.MAX_VALUE){
								tileBorder = 0;
							}
							
							props.put("tile", tileStr);
							props.put("tileBorder",String.valueOf(tileBorder));
			        		t.set(0,writer.write(union));
			        		t.set(1,new HashMap<String,String>(dataMap)); // TODO: Is it necessary?
			        		t.set(2,props);
			        		bagOut.add(t);
			        		//count++;
			        		
			        		geoms = null;
			        		list.clear();
				    	}
	        		}
	    		}
	    		//System.out.println("OUTPUT OBJECTS: " + count);
			}catch (Exception e) {
				throw new IOException("Caught exception generating Result " + e.getMessage());
			} 
    		
			return bagOut;
			
		} catch (Exception e) {
			throw new IOException("Caught exception processing input row, error - " + e.getMessage());
		}
		
	}
	
private static void computeSegmentation2(){
				
		
		MultiSegment currNeighbor = null;
		int auxPixel = -1;
		
		ArrayList <Integer> nbSeg= new ArrayList <Integer>();
		
		boolean hasmerged=true;
		int step=0;
		while (hasmerged && step < _maxSteps){
			hasmerged=false;
			
			int idSeg=0;
			//for (MultiSegment currSegment : _segmentsPtr2) {
			for  (Map.Entry<Integer, MultiSegment> entry : _segmentsPtr2.entrySet()){	
				
				//If it is a valid segment
				MultiSegment currSegment = entry.getValue();
				idSeg = entry.getKey();
			
			//for (int idSeg=0; idSeg<_segmentsPtr2.size();idSeg++){
			//	MultiSegment currSegment =_segmentsPtr2.get(idSeg);
				
				
				if (currSegment.getId() == idSeg){
					auxPixel = idSeg;

					/* for each outline pixel */
					while (auxPixel!=-1)
					{
						if (_segmentsPtr2.get(auxPixel).isBorderline()){
							int [] nb = getPixelIdFromNeighbors2(auxPixel);
							  
							for (int n=0;n<4;n++)
							{
								if (nb[n] != -1)
								{
									if (_segmentsPtr2.containsKey(nb[n])){
										nb[n] = _segmentsPtr2.get(nb[n]).getId();
										//if neighbor has lower id (compute just lower IDs)
										if (nb[n]<idSeg){
											//if it has not been selected yet
											if (!nbSeg.contains(nb[n])){
												nbSeg.add(nb[n]);
											}
										}
									}
								}
							}
						 }
						//next pixel
						 auxPixel = _segmentsPtr2.get(auxPixel).getNextPixelId();
					}
					
					//get merging costs
					for (int nbId: nbSeg){
						currNeighbor = _segmentsPtr2.get(nbId);
						double cost = GetCost2(currSegment, currNeighbor);
						//test for the current segment
						if (cost < currSegment.getMergingCost()){
							currSegment.setMergingCost(cost);
							currSegment.getBestNb().clear();
							currSegment.getBestNb().add(nbId);
						}
						else if (cost == currSegment.getMergingCost()){
							//LIST
							currSegment.getBestNb().add(nbId);
							//or UNIQUE
//							if (nbId < currSegment.getBestNb().get(0)){
//								currSegment.getBestNb().clear();
//								currSegment.getBestNb().add(nbId);
//							}
						}
						//test for the neighbor segment
						if (cost < currNeighbor.getMergingCost()){
							currNeighbor.setMergingCost(cost);
							currNeighbor.getBestNb().clear();
							currNeighbor.getBestNb().add(idSeg);
						} 
						else if (cost == currSegment.getMergingCost()){
							//LIST
							currNeighbor.getBestNb().add(idSeg);
							//or UNIQUE
//							if (idSeg < currNeighbor.getBestNb().get(0)){
//								currNeighbor.getBestNb().clear();
//								currNeighbor.getBestNb().add(nbId);
//							}
						}
					}
					nbSeg.clear();
				}
				//idSeg++;
			}
			//idSeg=0;	
			//***Merge mutual segments
			for  (Map.Entry<Integer, MultiSegment> entry : _segmentsPtr2.entrySet()){
			//for (MultiSegment currSegment : _segmentsPtr2) {
				MultiSegment currSegment = entry.getValue();
				idSeg = entry.getKey();
			//for (int idSeg=0; idSeg<_segmentsPtr2.size();idSeg++){
			//	MultiSegment currSegment =_segmentsPtr2.get(idSeg);
				if (idSeg == currSegment.getId()){
					if (currSegment.getMergingCost() <= _scale){
						//test for mutual
						for(int bestNb : currSegment.getBestNb() ){
							currNeighbor = _segmentsPtr2.get(bestNb);
							if (currNeighbor.getBestNb().contains(idSeg)){
								mergeSegment2(currSegment, currNeighbor); //merge Segments
								currNeighbor.getBestNb().clear();
								currNeighbor.setMergingCost(Double.MAX_VALUE);
								hasmerged = true;
								break;
							}
						}
					}
					currSegment.getBestNb().clear();
					currSegment.setMergingCost(Double.MAX_VALUE);
				}
				//idSeg++;
			}
			step++;
		}
		
	}
	
	public static int[] getPixelIdFromNeighbors2 (int pixelId){    
	    	
	    //int width = _imageW;
		//int height = _imageH;
		int width=_numTilesX*_tileSize;
		int height = _numTilesY*_tileSize;
			
		int x = pixelId % width;
		int y = pixelId / width;
		
		int [] neighb =new int[4];		

		neighb[0] = (y>0) ? ((y-1)*width) + x : -1; //north
		neighb[1] = (x>0) ? (y*width) + (x-1) : -1; //west
		neighb[2] = (y<(height-1)) ? ((y+1)*width) + x : -1; //south
		neighb[3] = (x<(width-1)) ? (y*width) + (x+1) : -1; //east

		return neighb;
	}
	
	public static double calcColorStats2 (MultiSegment obj, MultiSegment neighb)
	{
		double[] mean = new double[_nBands];
		double[] colorSum = new double[_nBands];
		double[] squarePixels = new double[_nBands];
		double[] stddev = new double[_nBands];
		
		double[] color_f = new double [_nBands];
		double color_h=0;
		
		double areaObj, areaNb, areaRes;
		
		areaObj = obj.getArea();
		areaNb =  neighb.getArea();
		areaRes = areaObj+areaNb;
		
		// calculates color factor per band and total
		color_h = 0;
		for (int b = 0; b < _nBands; b++)
		{
			mean[b] = ((obj.getAvg_colorByIndex(b) *areaObj)+(neighb.getAvg_colorByIndex(b)*areaNb))/areaRes;
			squarePixels[b] = (obj.getAvg_color_squareByIndex(b))+(neighb.getAvg_color_squareByIndex(b));	
			colorSum[b] = obj.getColor_sumByIndex(b) + neighb.getColor_sumByIndex(b);	
			stddev[b] = Math.sqrt(Math.abs(squarePixels[b] - 2*mean[b]*colorSum[b] + areaRes*mean[b]*mean[b])/areaRes);
						
			color_f[b] = _wBand[b] * ((areaRes*stddev[b]) - ((areaObj* obj.getStd_colorByIndex(b)) + (areaNb * neighb.getStd_colorByIndex(b))));
			//System.out.println("STD " + obj.getStd_colorByIndex(b));
			//System.out.println("STD DEV " + stddev[b]);
			color_h += color_f[b];
		}
		
		return color_h;
	}
	
	public static double CalcSpatialStats2 (MultiSegment obj, MultiSegment neighb){
		double areaObj, areaNb, areaRes;
		double spatial_h =0, smooth_f=0, compact_f =0;
		double perimObj, perimNb, perimRes;
		double bboxObjLen, bboxNbLen, bboxResLen;
		double [] bboxRes;
		
		areaObj = obj.getArea();
		areaNb =  neighb.getArea();
		areaRes = areaObj+areaNb;
		
		perimObj = obj.getPerimeter();
		perimNb = neighb.getPerimeter();
		
		if (areaRes<4){ /* valid only if pixel neighborhood==4 */
			if (areaRes ==2){
				perimRes=6;
			}
			else {
				perimRes=8;
			}
		}
		else{
			perimRes = calcPerimeter2(obj, neighb);
		}
		
		bboxObjLen = obj.getB_boxByIndex(2)*2 +  obj.getB_boxByIndex(3)*2;
		bboxNbLen =  neighb.getB_boxByIndex(2)*2 +  neighb.getB_boxByIndex(3)*2;
		bboxRes = calcBbox2(obj, neighb);		
		bboxResLen = bboxRes[2]*2 + bboxRes[3]*2;
		
		/* smoothness factor */
		smooth_f = (areaRes*perimRes/bboxResLen - 
		 (areaObj*perimObj/bboxObjLen + areaNb*perimNb/bboxNbLen));

		/* compactness factor */
		compact_f = (Math.sqrt(areaRes)*perimRes - 
		 (Math.sqrt(areaObj)*perimObj + Math.sqrt(areaNb)*perimNb));

		/* spatial heterogeneity */
		spatial_h = _wCmpt*compact_f + (1-_wCmpt)*smooth_f;


		return spatial_h;
	}
	
	public static double calcPerimeter2 (MultiSegment obj, MultiSegment neighb){
		double perimTotal;
		int idNb;
		int auxPixel;
		
		perimTotal = obj.getPerimeter() + neighb.getPerimeter();
		//choose segment with smaller perimeter
		if ( obj.getPerimeter() <= neighb.getPerimeter() )
		{
			auxPixel = obj.getId();
			idNb = neighb.getId();
		}
		else
		{
			auxPixel = neighb.getId();
			idNb = obj.getId();
		}
		
		// for each pixel from the smaller perimeter segment
		while ( auxPixel != -1){
			// just for borderline
			if ( _segmentsPtr2.get(auxPixel).isBorderline())
			{
				int [] nb = getPixelIdFromNeighbors2(auxPixel);

				 //verify the neighbor pixels
				 for (int i = 0; i < 4; i++ )
				 {
					   if ( nb[i] != -1 ) // if it is not image limit 
					   {
						   if (_segmentsPtr2.containsKey(nb[i])){
							   nb[i] = _segmentsPtr2.get(nb[i]).getId();
							   if (idNb == nb[i]){ //if it is part of the bigger
								   perimTotal =-2;  
							   }
						   }
					   }
				 }
			}
			auxPixel = _segmentsPtr2.get(auxPixel).getNextPixelId();
		}
		return perimTotal;
	}
	
	public static double[] calcBbox2 (MultiSegment obj, MultiSegment neighb){
			double [] bbox=new double[4];
			double minRowObj, maxRowObj, minColObj, maxColObj;
			double minRowNb, maxRowNb, minColNb, maxColNb;
			double minRowRes, maxRowRes, minColRes, maxColRes;
		 		   
			minRowObj = obj.getB_boxByIndex(0);
			maxRowObj = obj.getB_boxByIndex(0)+obj.getB_boxByIndex(2);
			minColObj = obj.getB_boxByIndex(1);
			maxColObj = obj.getB_boxByIndex(1)+obj.getB_boxByIndex(3);
			
			minRowNb = neighb.getB_boxByIndex(0);
			maxRowNb = neighb.getB_boxByIndex(0)+neighb.getB_boxByIndex(2);
			minColNb = neighb.getB_boxByIndex(1);
			maxColNb = neighb.getB_boxByIndex(1)+neighb.getB_boxByIndex(3);
		 
		   if (minRowObj < minRowNb) minRowRes=minRowObj; else minRowRes=minRowNb;
		   if (minColObj < minColNb) minColRes=minColObj; else minColRes=minColNb;
		   if (maxRowObj > maxRowNb) maxRowRes=maxRowObj; else maxRowRes=maxRowNb;
		   if (maxColObj > maxColNb) maxColRes=maxColObj; else maxColRes=maxColNb;
		 
		   bbox[0] = minRowRes;
		   bbox[1] = minColRes;
		   bbox[2] = maxRowRes-minRowRes;
		   bbox[3] = maxColRes-minColRes;
		   
		   return bbox;
	 }
	
	private static void mergeSegment2(MultiSegment obj, MultiSegment neighb){	
		double areaObj, areaNb, areaRes;
				
		areaObj = obj.getArea();
		areaNb =  neighb.getArea();
		areaRes = areaObj+areaNb;
		
		// calculates color factor per band and total
		for (int b = 0; b < _nBands; b++)
		{
			obj.setAvg_colorByIndex(((obj.getAvg_colorByIndex(b) *areaObj)+(neighb.getAvg_colorByIndex(b)*areaNb))/areaRes,b);
			obj.setAvg_color_squareByIndex((obj.getAvg_color_squareByIndex(b))+(neighb.getAvg_color_squareByIndex(b)),b);	
			obj.setColor_sumByIndex((obj.getColor_sumByIndex(b))+(neighb.getColor_sumByIndex(b)),b);			
			obj.setStd_colorByIndex(Math.sqrt(Math.abs(obj.getAvg_color_squareByIndex(b) 
					- 2*obj.getAvg_colorByIndex(b)*obj.getColor_sumByIndex(b) 
					+ areaRes*obj.getAvg_colorByIndex(b)*obj.getAvg_colorByIndex(b))/areaRes),b);
		}
	

		obj.setPerimeter(calcPerimeter2(obj, neighb));
		obj.setB_box(calcBbox2(obj, neighb));
		
		obj.setArea(areaRes);
		
		resetPixels2(obj, neighb);
	}
	
	
	private static void resetPixels2(MultiSegment obj, MultiSegment neighb){	
		int neighborId, segmentId;
		int auxPixel;
	
		neighborId = neighb.getId();
		segmentId = obj.getId();
	
		/* for each pixel of the neighbor segment to be merged */
		auxPixel = neighborId;
		while (auxPixel != -1)
		{
			 /* if it is outline pixel, check if that must be changed */
			 if (_segmentsPtr2.get(auxPixel).isBorderline()== true) /* curr_segment->area>6, only valid for pixel neighborhood==4 */
			 {
				 int [] nb = getPixelIdFromNeighbors2(auxPixel);
				 /* if pixel is surrounded by pixels of the same segment or of the merged neighbor, 
					it's no longer a border pixel */
				 int i=0;
				 while ((i<4)&&(i>-1)){
					 if (nb[i] == -1){ /* image limit */
						 i=-1; 
					 } else if (!_segmentsPtr2.containsKey(nb[i])){
						 i=-1; 
					 }else
						 {
						 nb[i] = _segmentsPtr2.get(nb[i]).getId();
						 if ((nb[i] != segmentId)&&(nb[i] != neighborId))
					     {
					    	 i=-1;
					     }
					     else
					     {
					    	 i++;
					     }
					 }
				 }
				 if (i!=-1) /* no longer outline pixel */
				 {
					 _segmentsPtr2.get(auxPixel).setBorderline(false);
				 }
			 }
			 
			/* changes the value of the pixel in the segment matrix (assign it to the current segment) */
			_segmentsPtr2.get(auxPixel).setId(segmentId);
			 
			auxPixel = _segmentsPtr2.get(auxPixel).getNextPixelId();
		}


		if (obj.getArea()>6) /* curr_segment->area>6, only valid for pixel neighborhood==4 */
		{
			 /* for each outline pixel of the current segment */
			auxPixel = segmentId;
			 while (auxPixel != -1)
			 {
			   if (_segmentsPtr2.get(auxPixel).isBorderline()==true)
			   {
				   int [] nb = getPixelIdFromNeighbors2(auxPixel);
					 /* if pixel is surrounded by pixels of the same segment or of the merged neighbor, 
						it's no longer a border pixel */
					 int i=0;
					 while ((i<4)&&(i>-1)){
						 if (nb[i] == -1){ /* image limit */
							 i=-1; 
						 } else if (!_segmentsPtr2.containsKey(nb[i])){
							 i=-1; 
						 }else {
							 nb[i] = _segmentsPtr2.get(nb[i]).getId();
							 if ((nb[i] != segmentId)&&(nb[i] != neighborId))
						     {
						    	 i=-1;
						     }
						     else
						     {
						    	 i++;
						     }
						 }
					     
					 }
					 if (i!=-1) /* no longer outline pixel */
				 {
						 _segmentsPtr2.get(auxPixel).setBorderline(false);
					 }
				 }
				 auxPixel = _segmentsPtr2.get(auxPixel).getNextPixelId();
			 }
		}
		
		// include pixel list of neighbor in the list of curr_segment
		int last =  obj.getLastPixelId();
		_segmentsPtr2.get(last).setNextPixelId(neighborId);
		obj.setLastPixelId(neighb.getLastPixelId());
	}

	
	public static double GetCost2 (MultiSegment segment, MultiSegment nbSegment){
		double cost =0;
		if (_wColor > 0){
			//spectral cost
			cost = calcColorStats2(segment, nbSegment) * _wColor;	
		}		
		if ((1- _wColor) > 0)
		{
			//morphological cost
			cost = cost + CalcSpatialStats2(segment, nbSegment) * (1-(_wColor));
		}
		return cost;
	}
	
	@Override
    public Schema outputSchema(Schema input) {
        
		try {

			List<Schema.FieldSchema> list = new ArrayList<Schema.FieldSchema>();
			list.add(new Schema.FieldSchema(null, DataType.CHARARRAY));
			list.add(new Schema.FieldSchema(null, DataType.MAP));
			list.add(new Schema.FieldSchema(null, DataType.MAP));
			
			Schema tupleSchema = new Schema(list);
			
			Schema.FieldSchema ts = new Schema.FieldSchema(null, tupleSchema, DataType.TUPLE);
			
			Schema bagSchema = new Schema(ts);
			
			Schema.FieldSchema bs = new Schema.FieldSchema(null, bagSchema, DataType.BAG);
			
			return new Schema(bs);

		} catch (Exception e) {
			return null;
		}
		
    }
	
}
