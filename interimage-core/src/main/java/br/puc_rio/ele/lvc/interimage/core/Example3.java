package br.puc_rio.ele.lvc.interimage.core;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.apache.pig.ExecType;
import org.apache.pig.PigServer;

import com.google.common.base.Joiner;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.precision.GeometryPrecisionReducer;
import com.vividsolutions.jts.simplify.TopologyPreservingSimplifier;

import br.puc_rio.ele.lvc.interimage.common.PigLogAnalyzer;
import br.puc_rio.ele.lvc.interimage.common.SFCTileManager;
import br.puc_rio.ele.lvc.interimage.common.SpatialIndex;
import br.puc_rio.ele.lvc.interimage.common.TileManager;
import br.puc_rio.ele.lvc.interimage.common.URL;
import br.puc_rio.ele.lvc.interimage.core.project.Project;
import br.puc_rio.ele.lvc.interimage.core.operatorgraph.PigParser;
import br.puc_rio.ele.lvc.interimage.core.operatorgraph.gClusterOperator;
import br.puc_rio.ele.lvc.interimage.core.operatorgraph.gController;
import br.puc_rio.ele.lvc.interimage.data.ImageConverter;
import br.puc_rio.ele.lvc.interimage.geometry.ShapefileConverter;

public class Example3 {
	
	public static void runRecursiveBaatz() {
		

		Locale locale = new Locale("en", "US");
		Locale.setDefault(locale);
		
		Project project = new Project();		
		project.readOldFile("C:\\Users\\patrick\\Documents\\interIMAGE2\\ICP\\exercise\\merged\\merged.gap", false);		
		//project.readOldFile("C:\\Users\\patrick\\Documents\\interIMAGEII\\ICP\\exercise\\tessioq1_4\\tessioq1_4.gap", false);
		/*Properties props = project.getProperties();
		
		gController g = new gController();
		g.setProperties(props);		
		
		gClusterOperator op1 = g.addClusterOperator();
		
		op1.setName("Segmentation");
		//op1.setOperatorName("RecursiveBaatz");
		op1.setOperatorName("RecursiveBaatzDissolve");
		op1.setProperties(props);
		
		Map<String,String> params = new HashMap<String, String>();
		params.put("$STORE","false");
						
		g.execute();*/
		
	}
	
	public static void test() throws Exception {
		 double[] _threshold;
		 String[] _class; 
		 int[] _bandsOperation;
		 double _minArea;

		 String thresholds = "-inf,0.7";
		 String classes ="vegetation" ;
		 String operation= "Index(1,2)";
		 String minArea = "0.5";
		 
		//thresholds
		String[] tr = thresholds.split(",");
		_threshold = new double [tr.length];
		for (int i=0; i< tr.length; i++) {
			if (tr[i].equals("-inf") ){
				_threshold[i]=-Double.MAX_VALUE;
			} else if (tr[i].equals("inf")){
				_threshold[i]=Double.MAX_VALUE;
			} else{
				_threshold[i] = Double.parseDouble(tr[i]);
			}
		}
		 
		_minArea = Double.parseDouble(minArea);		
				
		_class = classes.split(",");
		
		int tam = _class.length;
		int tam2 = (tr.length-1);
		
		if (_class.length != (tr.length-1)){
			throw new Exception("Problem with input thresholds and classes");
		}
		if (operation.contains("Expression")){
			//TODO: Parse expression
			_bandsOperation=null;
		} else if (operation.contains("Index")){
			try{
				String[] bands = operation.substring(operation.indexOf("(")+1, operation.indexOf(")")).split(",");
				_bandsOperation=new int[2];
				_bandsOperation[0] = Integer.parseInt(bands[0]);
				_bandsOperation[1] = Integer.parseInt(bands[1]);
			}catch (Exception e){
				throw new Exception("Problem with input operation");
			}
		}else if (operation.contains("Brightness")){
			_bandsOperation=null;
		} else if (operation.contains("Band")){
			String band = operation.substring(operation.indexOf("(")+1, operation.indexOf(")"));
			_bandsOperation=new int[1];
			_bandsOperation[0]=Integer.parseInt(band);
		} else {
			throw new Exception("Problem with input operation");
		}
		
		
		int i =0;
		/*Map<String,Object> objProperties = new HashMap<String,Object>() ;
		Map<String,Double> classification = null;
		
		String className = "Amianto";
		Double membership = 0.5;	
	
		classification = new HashMap<String,Double>();
		classification.put(className,membership);		
		objProperties.put("classification", classification);
		
		className = "Ceramica";
		membership = 0.8;	
	
		classification = (Map<String,Double>)objProperties.get("classification");
		classification.put(className,membership);
	
		String classNames = "Amianto,Ceramica,CeramicaEscura,SoloExposto,PavimentoConcreto,Piscina,Metal1,Metal2,Metal3,Metal4";
		Map<String,Double>_classPriorities = new HashMap<String,Double>();
		_classPriorities = new HashMap<String,Double>();
		String[] cs = classNames.split(",");
		for (int i=0; i<cs.length; i++) {
			_classPriorities.put(cs[i], (i+1)/((double)100000));
		}	
		
		
		if (objProperties.containsKey("classification")) {
			Map<String,Double> classification2 = (Map<String,Double>)objProperties.get("classification");
			
			Double membership2 = -1.0;
			String className2 = null;
			
			for (Map.Entry<String, Double> entry : classification2.entrySet()) {
				
				String c = entry.getKey();
				Double m = _classPriorities.get(c) + entry.getValue();
				
				if (m > membership2) {
					membership2 = m;
					className2 = c;
				}
			}
			
			objProperties.put("class", className2);
			objProperties.put("membership", membership2);
			
			System.out.println(objProperties.get("class").toString());
			System.out.println(objProperties.get("membership").toString());
			System.out.println(objProperties.get("classification").toString());
		}*/
	}
	
	
	public static void main(String[] args) throws IOException {
		
		//runRecursiveBaatz();
		
		try {
			test();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/*String []args2 = new String[4];
		args2[0]= "2"; //nodes
		args2[1]= "1024"; //tileSize
		args2[2] = "Thales_32k";//image
		args2[3] = "Thales_32k";//name*/
				
		/*ScriptPigDissolve.main(args2);
		ScriptPigRecursive.main(args2);
		ScriptPigNoRecursive.main(args2);*/
		
		/*args2[0]= "2"; //nodes
		ScriptPigDissolve.main(args2);
		ScriptPigRecursive.main(args2);
		ScriptPigNoRecursive.main(args2);
		
		args2[0]= "3"; //nodes
		ScriptPigDissolve.main(args2);
		ScriptPigRecursive.main(args2);
		ScriptPigNoRecursive.main(args2);
		
		args2[0]= "5"; //nodes
		ScriptPigDissolve.main(args2);
		ScriptPigRecursive.main(args2);
		ScriptPigNoRecursive.main(args2);
		
		args2[0]= "9"; //nodes
		ScriptPigDissolve.main(args2);
		ScriptPigRecursive.main(args2);
		ScriptPigNoRecursive.main(args2);
		
		args2[0]= "17"; //nodes
		ScriptPigDissolve.main(args2);
		ScriptPigRecursive.main(args2);
		ScriptPigNoRecursive.main(args2);
		
		args2[0]= "33"; //nodes
		ScriptPigDissolve.main(args2);
		ScriptPigRecursive.main(args2);
		ScriptPigNoRecursive.main(args2);*/
		
		
		/*System.out.println(Integer.MAX_VALUE);
		System.out.println(Math.pow(2, 31)-1);
		System.out.println(Math.pow(2, 32)-1);
		System.out.println(Math.pow(32000, 2));
		
		System.out.println(Integer.MAX_VALUE - (32000*32000));
		System.out.println(Integer.MAX_VALUE - (3969*512*512));*/
		
	}


}
