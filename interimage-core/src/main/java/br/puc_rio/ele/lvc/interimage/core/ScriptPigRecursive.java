package br.puc_rio.ele.lvc.interimage.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class ScriptPigRecursive {
	
	public static void main(String[] args) throws IOException {
		
		//String operator = "TBSPostProcesing_Baatz_Resegment";
		String operator = "Recursive";
		
		String scale = "50";
		String color = "0.8";
		String compact = "0.5";
		String bands = "1,1,1";
		
		String image = "image";
		String resolution = "0.5";
		String mutual = "1";
		String type = "1";
		String maxSteps = "1000";
		
		//int nodes = 5;
		int nodes = Integer.parseInt(args[0]);
		int elements = 8;
		int proc = (nodes-1)*elements;
		
		//String tileSize = "1024";
		String tileSize = args[1];
		//String imageName = "tessioq1_4";
		String imageName = args[2];
		String base = "s3n://interimageii/interimage/" + imageName + "/" + tileSize;
		String imageURL = "https://s3.amazonaws.com/interimageii/interimage/" + imageName + "/" + tileSize + "/resources/images/";
		String gridUrl = "https://s3.amazonaws.com/interimageii/interimage/" + imageName + "/" + tileSize + "/resources/tiles.ser";
		String result = base + "/results_ICP_Recursive_" + (nodes-1) + "/";

		String dir="C:\\Users\\patrick\\Documents\\InterimageII\\ICP\\exercise\\" + imageName + "\\tiles";
		//String output = "C:\\Users\\patrick\\Documents\\InterimageII\\ICP\\exercise\\" + imageName + "\\"+ operator + ".pig";
		String output = "C:\\Users\\patrick\\Documents\\InterimageII\\ICP\\exercise\\Scripts\\"+ (nodes-1) + "Core_" + args[3] + "_" + tileSize +
						"_" + operator + ".pig";
		BufferedWriter  writer = new BufferedWriter(new OutputStreamWriter( new FileOutputStream(output), "utf-8"));
				
	
		File file = new File(dir); 
		File afile[] = file.listFiles(); 
		int size = afile.length;
		
		int codeSize = afile[0].getName().length() - 5;
		
		int maxLevel=0;
		//Compute Max Level
		for (int i=0 ; i < size; i++) {
			String currStr = afile[i].getName();
			for (int j=i+1; j<size; j++){
				String tempStr = afile[j].getName();
				int level =0;
				for (int k=0; k< codeSize; k++){
					if (currStr.charAt(k)==tempStr.charAt(k)){
						level++;
					} 
					else {
						break;
					}
				}
				level = codeSize - level;
				if (maxLevel < level){
					maxLevel = level;
				}
			}
		}
		//System.out.println(maxLevel);
		
		int maxLoad = Math.min(Math.min(proc, size),127);
		
		//Write PIG HEADER
		writer.write("SET default_parallel " + maxLoad + ";");
		writer.newLine();
		writer.write("SET pig.tmpfilecompression.codec lzo;");
		writer.newLine();
		writer.write("SET pig.noSplitCombination false; -- true?;");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/jts-1.13.jar;");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/imglib2-2.0.0-beta-25.jar;");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/imglib2-algorithms-2.0.0-beta-25.jar;");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/imglib2-meta-2.0.0-beta-25.jar;");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/imglib2-ops-2.0.0-beta-25.jar;");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/jai_codec.jar;");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/jai_core-1.1.3.jar;");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/jai_imageio-1.1.jar;");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/guava-17.0.jar;");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/snappy-0.3.jar;");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/interimage-common-0.0.1-SNAPSHOT.jar;");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/interimage-datamining-0.0.1-SNAPSHOT.jar;");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/interimage-data-0.0.1-SNAPSHOT.jar;");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/interimage-geometry-0.0.1-SNAPSHOT.jar;");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/interimage-operators-0.0.1-SNAPSHOT.jar;");
		writer.newLine();
		writer.write("IMPORT 's3n://interimageii/interimage/scripts/interimage-import.pig';");
		writer.newLine();
		writer.write("DEFINE II_SUBSTRING br.puc_rio.ele.lvc.interimage.common.udf.Substring;");
		writer.newLine();
		writer.write("DEFINE II_MultiresolutionSegmentation br.puc_rio.ele.lvc.interimage.operators.udf.MultiresolutionSegmentationForRecursiveBaatz('"
				+  imageURL + "','"+ image + "','" + scale + "','" + color + "','" + compact + "','" + bands + "','" + mutual + "', '" + type + "', '" 
				+ maxSteps + "','"+ gridUrl + "','" + tileSize + "','" + resolution + "');");
		writer.newLine();
		writer.write("REGISTER s3n://interimageii/interimage/lib/jai_imageio-1.1.jar;");
		writer.newLine();
		
		//write "LOOP" DEFINE
		for (int i=1; i <= maxLevel; i++){
			writer.write("DEFINE II_SegmentationPostProcessing_" + i + " br.puc_rio.ele.lvc.interimage.operators.udf." + operator +"('"
					+  imageURL + "','"+ image + "','" + scale + "','" + color + "','" + compact + "','" + bands + "','" + gridUrl + "','" 
					+ resolution + "','"+ i + "');");
			writer.newLine();
		}
		
		//Split tiles to LOAD
		int perNode = size / proc;
		int rest = size % proc;
		//System.out.println(perNode);
		//System.out.println(rest);
		
		for (int n=0; n<maxLoad; n++) {
			writer.write ("load_" + (n+1) + " = LOAD '" + base + "/tiles/{");
			if (n<rest){
				for (int i=0 ; i < (perNode); i++) { 
					File arq = afile[i + n*(perNode+1)];
					writer.write(arq.getName() + ",");
				}
				File arq = afile[perNode + n*(perNode+1)];
				writer.write(arq.getName());
			}
			else {
				for (int i=0 ; i < perNode-1; i++) { 
					File arq = afile[i + n*perNode + rest];
					writer.write(arq.getName() + ",");
				}
				File arq = afile[perNode-1 + n*perNode + rest];
				writer.write(arq.getName());
			}
			
			writer.write("}' USING org.apache.pig.builtin.JsonLoader('geometry:chararray, data:map[chararray], properties:map[bytearray]');");
			writer.newLine();
		}
		
		//Initial Projections 
		for (int i=1; i <= maxLoad; i++){
			writer.write("projection_" + i + " = FOREACH load_" + i + " GENERATE FLATTEN(II_MultiresolutionSegmentation(geometry, data, properties)) AS (geometry:chararray, data:map[chararray], properties:map[bytearray]);");
			writer.newLine();
		}

		//Initial Splits & Store
		for (int i=1; i <= maxLoad; i++){
			writer.write("SPLIT  projection_" + i + " INTO save0" + i  + " IF (chararray) properties#'tileBorder' == '0'");
			for (int n=1; n <= maxLevel; n++){	
				writer.write(", proc" + n + "_" + i + " IF (chararray) properties#'tileBorder' == '" + n +"'");
			}
			writer.write(";");
			writer.newLine();
			writer.write("STORE save0"+ i + " INTO '" + result + "result0" + i + "' USING br.puc_rio.ele.lvc.interimage.common.udf.CompressedJsonStorage();");
			writer.newLine();
		}
		
		
		for (int n=1; n <= maxLevel; n++){
			//Cogroup
			writer.newLine();
			writer.write("byGroupId" + n + " = COGROUP ");
			for (int i=1; i < maxLoad; i++){
				writer.write(" proc" + n + "_" + i + " BY II_SUBSTRING(properties#'tile',0," + (codeSize-n-1) + "),");
			}
			writer.write(" proc" + n + "_" + maxLoad + " BY II_SUBSTRING(properties#'tile',0," + (codeSize-n-1) + ");");
			
			//Foreach
			writer.newLine();
			writer.write("res" + n + " = FOREACH byGroupId" + n +  " GENERATE FLATTEN(II_SegmentationPostProcessing_" + n + "(");
			for (int i=1; i < maxLoad; i++){
				writer.write("proc" + n + "_" + i  + ",");
			}
			writer.write(" proc" + n + "_" + maxLoad + ")) AS (geometry:chararray, data:map[chararray], properties:map[bytearray]);");
			writer.newLine();
			
			if (n<maxLevel){
			
				writer.write("SPLIT  res" + n + " INTO save" + n + " IF (chararray) properties#'tileBorder' == '0'");
				for (int l=(n+1); l <= maxLevel; l++){	
					writer.write(", proc_aux" + l + " IF (chararray) properties#'tileBorder' == '" + l +"'");
				}
				writer.write(";");
				writer.newLine();
				writer.write("STORE save" + n + " INTO '" + result + "result" + n + "' USING br.puc_rio.ele.lvc.interimage.common.udf.CompressedJsonStorage();");
				for (int l=(n+1); l <= maxLevel; l++){	
					writer.newLine();
					writer.write("proc" + l + "_1 = UNION proc" + l + "_1 , proc_aux" + l + ";");
				}
			} else{
				writer.write("STORE res" + n + " INTO '" + result + "result" + n + "' USING br.puc_rio.ele.lvc.interimage.common.udf.CompressedJsonStorage();");
			}
			
		}
		
		
		writer.close();
	}

}