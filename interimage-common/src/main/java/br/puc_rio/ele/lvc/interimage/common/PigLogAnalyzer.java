package br.puc_rio.ele.lvc.interimage.common;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class PigLogAnalyzer {

	Map<String, Map<String, Object>> _map = new HashMap<String, Map<String, Object>>();
	
	public void printLog() {
		
		long map_sum = 0;
		int map_count = 0;
		
		long reduce_sum = 0;
		int reduce_count = 0;
		
		long map_max = 0;
		long map_min = Long.MAX_VALUE;
		
		long reduce_max = 0;
		long reduce_min = Long.MAX_VALUE;
		
		
		for (Map.Entry<String, Map<String, Object>> entry : _map.entrySet()) {
			
			String key = entry.getKey();
			Map<String, Object> m = entry.getValue();			
			
			System.out.print(key + " ");
			
			Long finish = (Long)m.get("FINISH_TIME");
			Long start = (Long)m.get("START_TIME");
			
			String type = (String)m.get("TASK_TYPE");
			
			long d = finish-start;
			
			System.out.print(type + " ");
						
			if (type.equals("MAP")) {
				
				map_sum += d;
				
				if (d>map_max) {
					map_max = d; 
				}
				
				if (d<map_min) {
					map_min = d; 
				}
				
				map_count++;
				
			} else if (type.equals("REDUCE")) {
				
				reduce_sum += d;

				if (d>reduce_max) {
					reduce_max = d; 
				}
				
				if (d<reduce_min) {
					reduce_min = d; 
				}

				
				reduce_count++;
				
			}
			
			System.out.println("duration: " + ((d)/(double)1000));
			
		}
				
		System.out.println("map max: " + map_max/(double)1000);
		System.out.println("map min: " + map_min/(double)1000);
		
		System.out.println("reduce max: " + reduce_max/(double)1000);
		System.out.println("reduce min: " + reduce_min/(double)1000);		
		
		System.out.println("map avg duration: " + ((map_sum/(double)map_count)/(double)1000));
		System.out.println("reduce avg duration: " + ((reduce_sum/(double)reduce_count)/(double)1000));
		
	}
	
	public void readLog(String fileName) {
		
		try {
		
			BufferedReader br = new BufferedReader(new FileReader(fileName));
	
			String line = br.readLine();
			
			while (line != null) {
				
				String[] tokens = line.split(" ");
				
				String taskid = null;
				
				if (tokens[0].equals("Task")) {
					
					for (String field : tokens) {
						
						String[] t = field.split("=");
						
						if (t[0].equals("TASKID")) {
							
							taskid = t[1];
							
							if (!_map.containsKey(t[1])) {							
								_map.put(t[1], new HashMap<String, Object>());								
							}
						} else if (t[0].equals("START_TIME")) {
														
							Map<String, Object> m = _map.get(taskid);
							String aux = t[1].substring(1);
							aux = aux.substring(0, aux.length()-1);
							m.put(t[0],Long.valueOf(aux));							
														
						} else if (t[0].equals("FINISH_TIME")) {
														
							Map<String, Object> m = _map.get(taskid);
							String aux = t[1].substring(1);
							aux = aux.substring(0, aux.length()-1);
							m.put(t[0],Long.valueOf(aux));							
							
						} else if (t[0].equals("TASK_TYPE")) {
							
							Map<String, Object> m = _map.get(taskid);
							String aux = t[1].substring(1);
							aux = aux.substring(0, aux.length()-1);
							m.put(t[0],aux);							
														
						}
						
					}
					
				}
				
				line = br.readLine();
				
			}
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
				
	}
	
}
