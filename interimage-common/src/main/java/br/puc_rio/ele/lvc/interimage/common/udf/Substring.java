/*Copyright 2014 Computer Vision Lab

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package br.puc_rio.ele.lvc.interimage.common.udf;

import java.io.IOException;

import org.apache.pig.EvalFunc;
import org.apache.pig.PigWarning;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.logicalLayer.schema.Schema;

/**
 * SUBSTRING implements eval function to get a part of a string.
 * Example:<code>
 *      A = load 'mydata' as (name);
 *      B = foreach A generate SUBSTRING(name, 10, 12);
 *      </code>
 * First argument is the string to take a substring of.<br>
 * Second argument is the index of the first character of substring.<br>
 * Third argument is the index of the last character of substring.<br>
 * if the last argument is past the end of the string, substring of (beginIndex, length(str)) is returned.
 * @author Patrick Happ
 *
 */

public class Substring extends EvalFunc<String> {

    /**
     * Method invoked on every tuple during foreach evaluation
     * @param input tuple; first column is assumed to have the column to convert
     * @exception java.io.IOException
     */
    @Override
    public String exec(Tuple input) throws IOException {
        if (input == null || input.size() < 3) {
            warn("invalid number of arguments to SUBSTRING", PigWarning.UDF_WARNING_1);
            return null;
        }
        try {
            String source = DataType.toString(input.get(0));
            //System.out.println("INPUT: " + source);
            Integer beginindex = DataType.toInteger(input.get(1));
            Integer endindex = DataType.toInteger(input.get(2));
            //System.out.println("OUTPUT: " + source.substring(beginindex, Math.min(source.length(), endindex)));
            return source.substring(beginindex, Math.min(source.length(), endindex));
        } catch (NullPointerException npe) {
            warn(npe.toString(), PigWarning.UDF_WARNING_2);
            return null;
        } catch (StringIndexOutOfBoundsException npe) {
            warn(npe.toString(), PigWarning.UDF_WARNING_3);
            return null;
        } catch (ClassCastException e) {
            warn(e.toString(), PigWarning.UDF_WARNING_4);
            return null;
        }
    }

    @Override
    public Schema outputSchema(Schema input) {
        return new Schema(new Schema.FieldSchema(null, DataType.CHARARRAY));
    }
}

