/*Copyright 2014 Computer Vision Lab

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package br.puc_rio.ele.lvc.interimage.common;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.pig.impl.io.PigNullableWritable;

/**
 * Custom tile partitioner. 
 * @author Rodrigo Ferreira
 */

public class TilePartitioner extends Partitioner<PigNullableWritable, Writable> {
	
	@Override
	public int getPartition(PigNullableWritable key, Writable value, int numPartitions) {
		
		String k = (String)key.getValueAsPigType();
		
		if (Common.isNumeric(k)) {
		
			int ret = Integer.valueOf(k) % numPartitions;
			return ret;
			
		} else {
			
			return (key.hashCode() & Integer.MAX_VALUE) % numPartitions;
			
		}
		
	}
	
}